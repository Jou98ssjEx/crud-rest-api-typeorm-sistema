import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import cors  from 'cors';
import morgan from 'morgan';
import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from '../swagger.json';
import fileUpload  from 'express-fileupload';

require('dotenv').config();

export default class Server {

    private static _instance: Server;

    public app: express.Application;
    public port: number;
    private httpServer: http.Server;

    private constructor() {

        this.app = express();
        this.port = Number( process.env.PORT ) || 3000;
        this.httpServer = new http.Server( this.app );

    }

    public static get instance() {
        return this._instance || ( this._instance = new this() );
    }

    start( callback: Function ) {
        this.httpServer.listen( this.port, callback() );
    }

    public config(): void {
        this.app.use( bodyParser.urlencoded({ extended: true }) );
        this.app.use( bodyParser.json() );
        this.app.use( cors({ origin: true, credentials: true }) );
        this.app.use(morgan('dev'));
        this.app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));  
        // this.app.use(upload.array('img', 10));
        // this.app.use(fileUpload());
    }

}