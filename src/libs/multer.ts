import multer from "multer";
import path from "path";

export const imageFilter = function (req, file, cb) {
  // accept image only
  if (!file.originalname.match(/\.(jpg|jpeg|SVG|svg|png|gif|PNG|JPG|JPEG)$/)) {
    // req.fileValidationError = 'goes wrong on the mimetype';
    return cb(
      {
        message: "¡Solo se permiten archivos de imagen!",
        code: "NO_PERMITE_ARCHIVO",
      },
      false
    );
  }
  cb(null, true);
};
