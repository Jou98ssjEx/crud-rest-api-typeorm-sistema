import "reflect-metadata";
import Server from "./classes/server";

import { UsuarioRoutes } from "./routes/user.routes";
import { RolRoutes } from "./routes/rol.routes";
import { FamiliaRoutes } from "./routes/familia.routes";
import { PaisRoutes } from "./routes/pais.routes";
import { TipoPescaRoutes } from "./routes/tipoPesca.routes";
import { ArtePescaRoutes } from "./routes/artePesca.routes";
import { TipoEspecieRoutes } from "./routes/tipoEspecie.routes";
import { UploadRoutes } from "./routes/upload.routes";
import { EspecieRoutes } from "./routes/especie.routes";
import { LoginRoutes } from "./routes/login.routes";
import { NombreComercialRoutes } from "./routes/nombreComercial.routes";
import { NombreLocalRoutes } from "./routes/nombreLocal.routes";
import { FotoRoutes } from "./routes/foto.routes";
import { VedaRoutes } from "./routes/veda.routes";

import { MedidaRoutes } from "./routes/medida.routes";
import { LocationRoutes } from "./routes/location.routes";

import { createConnection } from "typeorm";
import { VedaActualRoutes } from "./routes/vedaActual.routes";

const server = Server.instance;

createConnection()
  .then(() => {
    // server
    server.start(() => {
      console.log(
        `Express server puerto ${server.port}: \x1b[32m%s\x1b[0m`,
        "online"
      );
    });
    console.log("Base de datos: \x1b[32m%s\x1b[0m", "online");
  })
  .catch((error) => console.log("Error al conectar la base de datos", error));

// Body Parse
// middlewawres
server.config();

// routes
server.app.use("/api/foto", new FotoRoutes().router);
server.app.use("/api/especie", new EspecieRoutes().router);
server.app.use("/api/upload", new UploadRoutes().router);
server.app.use("/api/familia", new FamiliaRoutes().router);
server.app.use("/api/rol", new RolRoutes().router);
server.app.use("/api/usuario", new UsuarioRoutes().router);
server.app.use("/api/pais", new PaisRoutes().router);
server.app.use("/api/tipoPesca", new TipoPescaRoutes().router);
server.app.use("/api/artePesca", new ArtePescaRoutes().router);
server.app.use("/api/especiePesca", new TipoEspecieRoutes().router);
server.app.use("/api/nombreComercial", new NombreComercialRoutes().router);
server.app.use("/api/nombreLocal", new NombreLocalRoutes().router);
server.app.use("/api/veda/actual", new VedaActualRoutes().router);
server.app.use("/api/veda", new VedaRoutes().router);
server.app.use("/api/login", new LoginRoutes().router);
server.app.use("/api/medida", new MedidaRoutes().router);
server.app.use("/api/location", new LocationRoutes().router);
