import jwt from 'jsonwebtoken';


export const verificaToken = (req:any, res:any, next:any ) => {
    const token = req.query.token;
    jwt.verify( token, String(process.env.SEED), (err:any, decoded:any) => {
        if( err ) {
            return res.status(401).json({
                ok: false,
                mensaje: 'El token no valido',
                errors: err
            });
        }   
        req.usuario = decoded.usuario;       
        next();        
    });
}
