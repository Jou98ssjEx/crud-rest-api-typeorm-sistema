import sharp from "sharp";

// export const resize = (req: any, res: any, next: any) => {
//   if (!req.files) {
//     console.log("aqui");
//     next();
//     return;
//   }

//   console.log(req.file);
//   //   const photo = await jimp.read(req.file.path);
//   //   await photo.resize(1024, jimp.AUTO);
//   //   await photo.write(`uploads/big/${req.file.filename}`);

//   next();
// };

export const resize = async (req, res, next) => {
  if (!req.files) return next();

  req.body.images = [];
  await Promise.all(
    req.files.map(async (file) => {
      const filename = file.originalname.replace(/\..+$/, "");
      const newFilename = `bezkoder-${filename}-${Date.now()}.jpeg`;

      await sharp(file.buffer)
        .resize(640, 320)
        .toFormat("jpeg")
        .jpeg({ quality: 90 })
        .toFile(`public/resize/${newFilename}`);

      req.body.images.push(newFilename);
    })
  );

  next();
};
