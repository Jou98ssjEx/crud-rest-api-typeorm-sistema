import { Router } from "express";
import { verificaToken } from "../middlewares/autenticacion";
import { resize } from "../middlewares/resize";

import { FotoController } from "../controllers/foto.controller";

export class FotoRoutes {
  public router: Router;
  public fotoController: FotoController = new FotoController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", this.fotoController.getFotos);
    this.router.get("/:tipo/:img", this.fotoController.getFoto);

    this.router.post("/:tipo/:id", this.fotoController.saveFoto);

    this.router.delete("/:id", this.fotoController.deleteFoto);
    // this.router.get('/:id', this.familiaController.getFamiliaId);
    // this.router.put('/:id', this.familiaController.updateFamilia);
    // this.router.delete('/:id', this.familiaController.deleteFamilia);
  }
}
