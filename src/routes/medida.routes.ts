import {Router} from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { MedidaController  } from '../controllers/medida.controller';

export class MedidaRoutes {
    public router: Router;
    public especieController: MedidaController = new MedidaController();

    constructor() {
        this.router = Router()
        this.routes();
    }

    routes() {
        this.router.get('/', this.especieController.getMedidas);
        this.router.get('/:id', this.especieController.getMedidaId);
        this.router.put('/:id', this.especieController.updateMedida);
        this.router.delete('/:id', this.especieController.deleteMedida);
        this.router.post('/', this.especieController.createMedida);
    }
}