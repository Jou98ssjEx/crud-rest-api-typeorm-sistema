import {Router} from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { LocationController  } from '../controllers/location.controller';

export class LocationRoutes {
    public router: Router;
    public especieController: LocationController = new LocationController();

    constructor() {
        this.router = Router()
        this.routes();
    }

    routes() {
        this.router.get('/', this.especieController.getLocation);
        this.router.get('/:id', this.especieController.getLocationId);
        this.router.put('/:id', this.especieController.updateLocation);
        this.router.delete('/:id', this.especieController.deleteLocation);
        this.router.post('/', this.especieController.createLocation);
    }
}