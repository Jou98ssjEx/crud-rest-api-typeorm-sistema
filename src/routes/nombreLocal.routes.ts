import { Router } from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { NombreLocalController } from '../controllers/nombreLocal.controller';

export class NombreLocalRoutes {
    
    public router: Router;
    public nombreLocalController: NombreLocalController = new NombreLocalController();

    constructor() {
        this.router = Router()
        this.routes();
    }

    routes() {

        this.router.get('/',  this.nombreLocalController.getNombresLocales);
        this.router.get('/:id', this.nombreLocalController.getNombreLocal);
        this.router.post('/',  this.nombreLocalController.createNombreL);
        this.router.put('/:id',  this.nombreLocalController.updateNombreL);
        this.router.delete('/:id',  this.nombreLocalController.deleteNombreL);
    }

}
