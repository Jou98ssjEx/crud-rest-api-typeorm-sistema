import { Router } from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { LoginController } from '../controllers/login.controller';

export class LoginRoutes {

    public router: Router;
    public loginController: LoginController = new LoginController();

    constructor() {
        this.router = Router()
        this.routes();
    }

    routes() {
        //Login
        this.router.post('/', this.loginController.loginUser);
        this.router.get('/renuevatoken', verificaToken, this.loginController.renuevaToken);
    }

}