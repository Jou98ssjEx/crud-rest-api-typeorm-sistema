import {Router} from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { PaisController } from '../controllers/pais.controller';


export class PaisRoutes {
    public router: Router;
    public paisController: PaisController = new PaisController();

    constructor() {
        this.router = Router()
        this.routes();
    }

    routes() {

        this.router.get('/',  this.paisController.getPais);
        this.router.get('/:id',  this.paisController.getPaisId);
        this.router.put('/:id',  this.paisController.updatePais);
        this.router.delete('/:id',  this.paisController.deletePais);
        this.router.post('/',  this.paisController.createPais);
        }
}


