import { Router } from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { NombreComercialController } from '../controllers/nombreComercial.controller';

export class NombreComercialRoutes {
    
    public router: Router;
    public nombreComercialController: NombreComercialController = new NombreComercialController();

    constructor() {
        this.router = Router()
        this.routes();
    }

    routes() {

        this.router.get('/',  this.nombreComercialController.getNombresComerciales);
        this.router.get('/:id', this.nombreComercialController.getNombreComercial);
        this.router.post('/',  this.nombreComercialController.createNombreC);
        this.router.put('/:id',  this.nombreComercialController.updateNombreC);
        this.router.delete('/:id',  this.nombreComercialController.deleteNombreC);
    }

}
