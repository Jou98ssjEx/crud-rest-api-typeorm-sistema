import { Router } from "express";
import { VedaActualController } from "../controllers/vedaActual.controller";

export class VedaActualRoutes {
  public router: Router;
  public vedaActualController: VedaActualController = new VedaActualController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", this.vedaActualController.getVeda);
  }
}
