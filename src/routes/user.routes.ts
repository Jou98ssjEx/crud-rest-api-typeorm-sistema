import { Router } from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { UsuarioController } from '../controllers/user.controller';

export class UsuarioRoutes {
    
    public router: Router;
    public usuarioController: UsuarioController = new UsuarioController();

    constructor() {
        this.router = Router()
        this.routes();
    }

    
    routes() {

        this.router.get('/', this.usuarioController.getUsers);
        this.router.get('/:id', this.usuarioController.getUser);
        this.router.post('/',  this.usuarioController.createUsers);
        this.router.put('/:id', this.usuarioController.updateUser);
        this.router.put('/recupera/:id', this.usuarioController.updatePass);
        this.router.delete('/:id', this.usuarioController.deleteUser);
        
    }

}
