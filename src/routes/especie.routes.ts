import { Router } from "express";
import { verificaToken } from "../middlewares/autenticacion";
import { EspecieController } from "../controllers/especie.controller";
var cron = require("node-cron");

export class EspecieRoutes {
  public router: Router;
  public especieController: EspecieController = new EspecieController();

  constructor() {
    this.router = Router();
    this.routes();
    this.especieController.ejemplo();
  }

  routes() {
    this.router.get("/", this.especieController.getEspecies);
    this.router.get("/:id", this.especieController.getEspecieId);
    this.router.put("/:id", this.especieController.updateEspecie);
    this.router.delete("/:id", this.especieController.deleteEspecie);
    this.router.post("/", this.especieController.createEspecie);
    // this.router.get('/:id', this.familiaController.getFamilia);
    // this.router.post('/', this.familiaController.createFamilia);
    // this.router.put('/:id', this.familiaController.updateFamilia);
    // this.router.delete('/:id', this.familiaController.deleteFamilia);
  }
}
