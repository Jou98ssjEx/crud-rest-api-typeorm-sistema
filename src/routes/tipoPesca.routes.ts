import { Router } from "express";
import { verificaToken } from "../middlewares/autenticacion";
import { TipoPescaController } from "../controllers/tipoPesca.controller";
export class TipoPescaRoutes {
  public router: Router;
  public tipoPController: TipoPescaController = new TipoPescaController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", this.tipoPController.getTipoPesca);
    this.router.get("/:id", this.tipoPController.getTipoPescaId);
    this.router.put("/:id", this.tipoPController.updateTipoPesca);
    this.router.delete(
      "/:id",
      verificaToken,
      this.tipoPController.deleteTipoPesca
    );
    this.router.post("/", this.tipoPController.createTipoPesca);
  }
}
