import {Router} from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { FamiliaController } from '../controllers/familia.controller';

export class FamiliaRoutes {
    public router: Router;
    public familiaController: FamiliaController = new FamiliaController();

    constructor() {
        this.router = Router()
        this.routes();
    }

    routes() {
        this.router.get('/', this.familiaController.getFamilias);
        this.router.post('/', this.familiaController.createFamilia);
        this.router.get('/:id', this.familiaController.getFamiliaId);
        this.router.put('/:id', this.familiaController.updateFamilia);
        this.router.delete('/:id', this.familiaController.deleteFamilia);
    }
}