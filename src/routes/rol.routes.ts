import {Router} from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { RolController } from '../controllers/rol.controller';


export class RolRoutes {
    public router: Router;
    public rolController: RolController = new RolController();

    constructor() {
        this.router = Router()
        this.routes();
    }

    routes() {

        this.router.get('/',  this.rolController.getRols);
        // this.router.post('/', verificaToken, this.rolController.createRols);
        // this.router.get('/:id', verificaToken, this.rolController.getRol);
        // this.router.put('/:id', verificaToken, this.rolController.updateRol);
        // this.router.delete('/:id', verificaToken, this.rolController.deleteRol);
    }
}


