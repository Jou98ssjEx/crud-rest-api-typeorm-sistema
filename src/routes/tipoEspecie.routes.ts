import {Router} from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { TipoPescaController } from '../controllers/tipoEspecie.controller';


export class TipoEspecieRoutes {
    public router: Router;
    public tipoPController: TipoPescaController = new TipoPescaController();

    constructor() {
        this.router = Router()
        this.routes();
    }

    routes() {

        this.router.get('/', this.tipoPController.getTipoEspecie);
        this.router.get('/:id', this.tipoPController.getTipoEspecieId);
        this.router.put('/:id', this.tipoPController.updateTipoEspecie);
        this.router.delete('/:id', this.tipoPController.deleteTipoEspecie);
        this.router.post('/', this.tipoPController.createTipoEspecie);
        }
}


