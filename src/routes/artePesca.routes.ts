import {Router} from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { ArtePescaController } from '../controllers/artePesca.controller';


export class ArtePescaRoutes {
    public router: Router;
    public artePController: ArtePescaController = new ArtePescaController();

    constructor() {
        this.router = Router()
        this.routes();
    }

    routes() {

        this.router.get('/',  this.artePController.getArtePesca);
        this.router.get('/:id',  this.artePController.getArtePescaId);
        this.router.put('/:id',  this.artePController.updateArtePesca);
        this.router.delete('/:id', verificaToken, this.artePController.deleteArtePesca);
        this.router.post('/',  this.artePController.createArtePesca);
        }
}


