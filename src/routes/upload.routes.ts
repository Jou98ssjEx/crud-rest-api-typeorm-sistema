import {Router} from 'express';
import { verificaToken } from '../middlewares/autenticacion';
import { UploadController } from '../controllers/upload.controller';

export class UploadRoutes {
    public router: Router;
    public uploadController: UploadController = new UploadController();
    
    constructor() {
        this.router = Router()
        this.routes();
    }

    routes() {
        
        this.router.put('/:tipo/:id', this.uploadController.updateFoto);
        this.router.get('/:tipo/:img', this.uploadController.getFoto);
    }
}