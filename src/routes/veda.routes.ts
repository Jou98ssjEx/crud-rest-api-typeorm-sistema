import { Router } from "express";
import { verificaToken } from "../middlewares/autenticacion";
import { VedaController } from "../controllers/veda.controller";

export class VedaRoutes {
  public router: Router;
  public vedaController: VedaController = new VedaController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", this.vedaController.getVeda);
    this.router.get("/:id", this.vedaController.getVedaId);
    this.router.put("/:id", this.vedaController.updateVeda);
    this.router.delete("/:id", this.vedaController.deleteVeda);
    this.router.post("/", this.vedaController.createVeda);
  }
}
