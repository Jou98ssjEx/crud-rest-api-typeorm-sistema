import { Request, Response } from "express";
import { getRepository, createQueryBuilder } from "typeorm";
import { validate } from "class-validator";
import { Especie } from "../entity/especie";
import { Paises } from "../entity/pais";
import { NombreLocal } from "../entity/nombre_local";
import { Veda } from "../entity/veda";
import moment from "moment";
var cron = require("node-cron");
var request = require("request");

export class EspecieController {
  // Metodo para obtener todas las especies
  public async getEspecies(req: Request, res: Response): Promise<Response> {
    try {
      // Se realiza un SELECT con unos inner Join para obtener
      // los datos de las tablas relacionadas
      // en este caso se obtendras los datos de todas las
      // especies registradas en la base de datos
      const especie = await getRepository(Especie).find({
        join: {
          alias: "especie",
          innerJoinAndSelect: {
            familia: "especie.familia",
            artePesca: "especie.artePesca",
            tipoPesca: "especie.tipoPesca",
            tipoEspecie: "especie.tipoEspecie",
          },
          leftJoinAndSelect: {
            foto: "especie.fotos",
            nombreComercial: "especie.nombreComercial",
            nombreLocal: "especie.nombreLocal",
            paises: "nombreLocal.paises",
            veda: "especie.veda",
            location: "especie.location",
            medida: "especie.medida", // agg tabla medida
          },
        },
      });

      // Se valida si el objeto de especia
      // en caso de que el objeto viene vacio es por que
      // a ocurrido un ERROR al obtener los datos
      // y un mesaje "ERROR al buscar las especies"
      //con estatus 400.
      if (!especie) {
        return res.status(400).json({
          ok: false,
          mensaje: "ERROR al buscar las especies",
        });
      }

      // Se valida el estado de la veda en caso de que la
      // veda este a punto de terminar o en fechas validas
      // en que la especie esta en veda solo se enviara la fecha de
      // veda por especie.
      especie.forEach((e, i, a) => {
        const start = moment(moment(), "YYYY-MM-DD");
        e.veda.map((d) => {
          var end = moment(d.inicio, "YYYY-MM-DD");
          const estaVeda = moment(start).isBetween(d.inicio, d.fin);
          if (estaVeda) {
            d.isVeda = true;
          }
        });
        let result = e.veda.filter((v) => v.isVeda === true);
        e.veda = result;
      });

      // En caso de no ocurrir un ERROR se responde con un objeto
      // de todas las especies con estatus 200
      return res.json({
        ok: true,
        especies: especie,
      });
    } catch (error) {
      // En caso de que ocurra un error del servidor
      // se devuelve un estatus 500 y un mensaje de
      // "ERROR interno del servidor"
      return res.status(500).json({
        ok: false,
        mensaje: "ERROR interno del servidor",
        error,
      });
    }
  }
  // Metodo para obtener una especie en especifico por medio del ID
  public async getEspecieId(req: Request, res: Response): Promise<Response> {
    // Se almacena el ID que se envia por los PARAMS
    const id = req.params.id;
    try {
      // Al igual al metodo anterior se realiza la consulta en este caso
      // sera una consulta individual por lo que se le realiza una condicion
      // WHERE por ID para obtener el dato que el cliente desea.
      const especieID = await getRepository(Especie).findOne({
        where: {
          id,
        },
        join: {
          alias: "especie",
          innerJoinAndSelect: {
            familia: "especie.familia",
            artePesca: "especie.artePesca",
            tipoPesca: "especie.tipoPesca",
            tipoEspecie: "especie.tipoEspecie",
            medida: "especie.medida", // agg tabla medida
          },
          leftJoinAndSelect: {
            foto: "especie.fotos",
            nombreComercial: "especie.nombreComercial",
            nombreLocal: "especie.nombreLocal",
            paises: "nombreLocal.paises",
            veda: "especie.veda",
            location: "especie.location",
          },
        },
      });

      // Se valida si el objeto de especiaID
      // en caso de que el objeto viene vacio es por que
      // a ocurrido un ERROR al obtener los datos o
      // en la BD no existe ese datos, se mostrara el
      // mesaje "No existe una especie de pesca con ese id"
      // con estatus 400.
      if (!especieID) {
        return res.status(400).json({
          ok: false,
          mensaje: "No existe una especie de pesca con ese id",
          errors: { message: "No existe una especie de pesca con ese id." },
        });
      }

      // Se consulta individualmente sobre la tabla VEDA para obtener todoas
      // las vedas de esta especia para despues ser manipulada.
      const veda = await getRepository(Veda)
        .createQueryBuilder("veda")
        .where("veda.especieId = :id", { id })
        .getMany();

      // Se consulta los nombre locales para obtener los paises
      // de cada especie.
      const nombreLocal = await createQueryBuilder("Paises")
        .leftJoinAndSelect("Paises.nombreLocal", "NombreLocal")
        .leftJoinAndSelect("NombreLocal.especie", "Especie")
        .where("Especie.id = :id", { id })
        .getMany();

      // Se valida el estado de la veda en caso de que la
      // veda este a punto de terminar o en fechas validas
      // en que la especie esta en veda solo se enviara la fecha de
      // veda por especie.
      var start = moment(moment(), "YYYY-MM-DD");
      const vedas = veda.map((d) => {
        var end = moment(d.inicio, "YYYY-MM-DD");
        const estaVeda = moment(start).isBetween(d.inicio, d.fin);
        const estaCerca = moment.duration(end.diff(start)).asDays();
        if (estaCerca > 0 && estaCerca < 5) {
          d.isVeda = true;
        }
        if (estaVeda) {
          d.isVeda = true;
        }
        return d;
      });

      // Se valida si existe una veda actual en caso de no existir
      // se resoponde con una VEDAACTUAL con fechas de 0000-00-00
      // pero despues todos los datos van iguales
      let resultado = vedas.find((v) => v.isVeda === true);
      if (!resultado) {
        return res.status(200).json({
          ok: true,
          especie: especieID,
          nombreLocal,
          vedaActual: {
            inicio: new Date("0000-00-00"),
            fin: new Date("0000-00-00"),
            isVeda: false,
          },
        });
      }
      // En caso de que todas las validaciones pase
      // se retorna un status 200 y el objeto con los datos especificos
      // de la escpecie
      return res.status(200).json({
        ok: true,
        especie: especieID,
        nombreLocal,
        vedaActual: resultado,
      });
    } catch (error) {
      // En caso de que ocurra un error del servidor
      // se devuelve un estatus 500 y un mensaje de
      // "ERROR interno del servidor"
      return res.status(500).json({
        ok: false,
        mensaje: "ERROR interno del servidor",
        error,
      });
    }
  }

  public async createEspecie(req: Request, res: Response): Promise<Response> {
    const body = req.body;

    const especie = new Especie();

    especie.dist_geo = body.geo;
    especie.talla = body.talla;
    especie.img = body.img;
    especie.importancia = body.importancia;
    especie.familia = body.familia;
    especie.artePesca = body.artePesca;
    especie.tipoPesca = body.tipoPesca;
    especie.tipoEspecie = body.tipoEspecie;
    especie.nombre_comun = body.nombreComun;
    especie.medida = body.medida;

    const errors = await validate(especie);

    if (errors.length > 0) {
      return res.status(400).json({
        ok: false,
        mensaje: "ERROR al crear la especie de pesca",
        mesajeError: errors[0].constraints.isNotEmpty,
        errors,
      });
    } else {
      try {
        const datosGuardados = await getRepository(Especie).save(especie);
        return res.status(201).json({
          ok: true,
          especie: datosGuardados,
        });
      } catch (error) {
        return res.status(400).json({
          ok: false,
          err: error,
        });
      }
    }
  }

  public async updateEspecie(req: Request, res: Response): Promise<Response> {
    const id = req.params.id;
    const body = req.body;

    const especie = await getRepository(Especie).findOne(id);

    if (!especie) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe una especie de pesca con ese id",
        errors: { message: "No existe una especie de pesca con ese id." },
      });
    }

    const especieAdd = new Especie();
    especie.dist_geo = body.geo;
    especie.talla = body.talla;
    especie.importancia = body.importancia;
    especie.familia = body.familia;
    especie.artePesca = body.artePesca;
    especie.tipoPesca = body.tipoPesca;
    especie.tipoEspecie = body.tipoEspecie;
    especie.medida = body.medida;
    especie.nombre_comun = body.nombreComun;

    const errors = await validate(especie);

    if (errors.length > 0) {
      return res.status(400).json({
        ok: false,
        mensaje: "ERROR al actualizar la especie de pesca",
        mesajeError: errors[0].constraints.isNotEmpty,
        errors,
      });
    } else {
      try {
        getRepository(Especie).merge(especie, especieAdd);
        const datos = await getRepository(Especie).save(especie);
        return res.status(200).json({
          ok: true,
          TipoEspecie: datos,
        });
      } catch (error) {
        return res.status(400).json({
          ok: false,
          err: error,
        });
      }
    }
  }

  public async deleteEspecie(req: Request, res: Response): Promise<Response> {
    const ER_ROW_IS_REFERENCED_2 = "ER_ROW_IS_REFERENCED_2";
    const id = req.params.id;
    const especie = await getRepository(Especie).findOne(id);
    if (!especie) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe una especie con id",
        errors: { message: "No existe una especie con ese id." },
      });
    }

    try {
      await getRepository(Especie).delete(especie.id);

      return res.status(200).json({
        ok: false,
        mensaje: "La especie de pesca a sido eliminado",
        especie,
      });
    } catch (error) {
      if (error.code === ER_ROW_IS_REFERENCED_2) {
        return res.status(400).json({
          ok: false,
          mensaje: "ERROR el tipo de especie esta vinculado con otra tabla",
          error:
            "No se puede eliminar o actualizar una fila principal: falla una restricción de clave externa",
          err: error,
        });
      }

      return res.status(400).json({
        ok: false,
        err: error,
      });
    }
  }

  public async ejemplo() {
    cron.schedule("00 07 * * *", async () => {
      try {
        const especie = await getRepository(Especie).find({
          join: {
            alias: "especie",
            leftJoinAndSelect: {
              veda: "especie.veda",
            },
          },
        });

        especie.forEach((item) => {
          const start = moment().format("MM/DD/YYYY");
          if (item.veda.length > 0) {
            item.veda.map((v) => {
              if (start === moment(v.inicio).format("L")) {
                this.notificacionFCM(
                  `La especie ${
                    item.nombre_comun
                  }, ingreso a veda, su fecha de finalización es ${moment(
                    v.fin
                  ).format("DD/MM/YYYY")}`
                );
              } else if (start === moment(v.fin).format("L")) {
                this.notificacionFCM(
                  `La especie ${item.nombre_comun} finalizo su veda, ahora la puedes consumir.`
                );
              }
            });
          }
        });
      } catch (error) {
        console.log("Ocurrio un error");
      }
    });
  }

  private notificacionFCM(body: string) {
    const data = {
      to: "/topics/see_pesca.ministerio",
      notification: {
        title: "Notificación de veda",
        body,
      },
      data: {},
    };

    request(
      {
        url: process.env.URL_FCM,
        method: "POST",
        headers: {
          "content-type": "application/json",
          Authorization: process.env.TOKEN_FCM,
        },
        body: JSON.stringify(data),
      },
      function (error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log(body);
        }
      }
    );
  }
}
