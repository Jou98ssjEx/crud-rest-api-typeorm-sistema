import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { validate } from 'class-validator';
import { Familia } from '../entity/familia';

export class FamiliaController {

    public async getFamilias(req: Request, res: Response): Promise<Response> {

        try {
            const familia = await getRepository(Familia).find();
            if( !familia ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'ERROR al buscar las familias'
                });
            }
            return res.json({
                ok: true,
                familias: familia
            });
        } catch (error) {
            return res.status(500).json({
                ok: true,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
 
    }

    public async getFamiliaId(req: Request, res: Response): Promise<Response> {
    
        try {
            const familiaId = await getRepository(Familia).findOne(req.params.id); 

            if( !familiaId ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'No existe una familia con ese id',
                    errors: {message: 'No existe una familia con ese id.'}
                });
            }

            return res.status(200).json({
                ok: true,
                Familia: familiaId
            });

        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
    }
    
    public async createFamilia(req: Request, res: Response): Promise<Response> {

        const body = req.body;
    
        const familia = new Familia();
        
        familia.nombre = body.nombre;
    
        const errors = await validate(familia);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al crear el arte de pesca',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                const datosGuardados = await getRepository(Familia).save(familia);
                return res.status(201).json({
                        ok: true,
                        Familia: datosGuardados
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
    }

    public async updateFamilia(req: Request, res: Response): Promise<Response> {

        const id = req.params.id;
        const body = req.body;

        const familia = await getRepository(Familia).findOne(id);

        if( !familia ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe una familia con ese id',
                errors: {message: 'No existe una familia con ese id.'}
            });
        }

        const familiaAdd = new Familia();
        familia.nombre = body.nombre;

        const errors = await validate(familia);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al actualizar el arte de pesca',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                getRepository(Familia).merge(familia, familiaAdd);
                const datos = await getRepository(Familia).save(familia);
                return res.status(200).json({
                    ok: true,
                    familia: datos
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
        
    }

    public async deleteFamilia(req: Request, res: Response): Promise<Response> {
        const ER_ROW_IS_REFERENCED_2 = 'ER_ROW_IS_REFERENCED_2';
        const id = req.params.id;
        const familia = await getRepository(Familia).findOne(id);
        if( !familia ){
            return res.status(400).json({
                ok: true,
                mensaje: 'No existe la familia con este id',
                errors: {message: 'No existe la familia con este id.'}
            });
        }

        try {

            await getRepository(Familia).delete(familia.id);  
            
            return res.status(200).json({
                ok: false,
                mensaje: 'El arte de pesca a sido eliminado',
                familia     
            });

        } catch (error) {
            if(error.code === ER_ROW_IS_REFERENCED_2 ) {
                return res.status(400).json({
                        ok: false,
                        mensaje: 'ERROR el tipo de pesca esta vinculado con otra tabla',
                        error: 'No se puede eliminar o actualizar una fila principal: falla una restricción de clave externa',
                        err: error
                });
            }
            return res.status(400).json({
                ok: false,
                err: error
            });
        }
     
    }

}