import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { User } from '../entity/user';
import { validate } from 'class-validator';

export class LoginController {

    public async loginUser(req: Request, res: Response): Promise<Response> {
        const loginData = req.body;
    
        const user = await getRepository(User).findOne({email_usuario: loginData.email_usuario}, {relations: ["rol"]}); 
    
        if (user && bcrypt.compareSync( loginData.pass_usuario, user.pass_usuario )) {
    
            const token = jwt.sign({ usuario: user }, String(process.env.SEED), { expiresIn: 14400 }); // 4 horas
    
            user.pass_usuario = ':)';
            
            return res.status(200).json({
                ok: true,
                usuario: user,
                token: token,
                id: user.id,
                menu: obtenerMenu(user.rol.role)
            });
        } else {
            return res.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrectas'
            });
        }
    
    }

    public async renuevaToken(req: any, res: Response): Promise<Response> {

        var token = jwt.sign({ usuario: req.usuario }, String(process.env.SEED), { expiresIn: 14400 }); // 4 horas

        return res.status(200).json({
            ok: true,
            token: token
        });
    }

}

export function obtenerMenu(ROLE: any) {

    const menu = [
        {
            titulo: 'Principal',
            icono: 'mdi mdi-gauge',
            submenu: [
                { titulo: 'Dashboard', url: '/dashboard' },
                { titulo: 'ProgressBar', url: '/progress' },
                { titulo: 'Gráficas', url: '/graficas1' },
                { titulo: 'Promesas', url: '/promesas' },
                { titulo: 'RxJs', url: '/rxjs' }
            ]
        }
    ];

    if (ROLE === 'ADMIN_ROLE') {
        menu.unshift(
            {
                titulo: 'Mantenimientos',
                icono: 'mdi mdi-folder-lock-open',
                submenu: [
                    { titulo: 'Usuarios', url: '/usuarios' }
                ]
            }
        );
    }
    
    return menu;

}