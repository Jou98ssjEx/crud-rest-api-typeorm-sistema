import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { NombreLocal } from '../entity/nombre_local';
import { validate } from 'class-validator';

export class NombreLocalController {
    
    public async getNombresLocales(req: Request, res: Response): Promise<Response> {

        try {
            const nombresL = await getRepository(NombreLocal).find({ relations: ["especie", "paises"], 
            select: ['nombre_local' ,'id'] });

            if( !nombresL ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'ERROR al buscar los nombres locales existentes'
                });
            }

            return res.status(200).json({
                ok: true,
                nombresLocales: nombresL
            });
        } catch (error) {
            return res.status(500).json({
                ok: true,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
        
    }

    public async getNombreLocal(req: Request, res: Response): Promise<Response> {
    
        try {
            const nombreLoId = await getRepository(NombreLocal).findOne(req.params.id, { relations: ["especie", "pais"],
            select: ['nombre_local', 'id'] }); 

            if( !nombreLoId ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'No existe un nombre local con ese id',
                    errors: {message: 'No existe un nombre local con ese id.'}
                });
            }

            return res.status(200).json({
                ok: true,
                NombreLocal: nombreLoId
            });

        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
        
        
    }

    public async createNombreL(req: Request, res: Response): Promise<Response> {

        const ER_DUP_ENTRY = 'ER_DUP_ENTRY';

        const body = req.body;
    
        const nombreLocal = new NombreLocal();
        nombreLocal.nombre_local = body.nombreLocal;
        nombreLocal.paises = body.paisId;
        nombreLocal.especie = body.especieId;
        const errors = await validate(nombreLocal);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al crear nombre local',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                const datosGuardados = await getRepository(NombreLocal).save(nombreLocal);
                return res.status(201).json({
                        ok: true,
                        NombreLocal: datosGuardados
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
    }

    public async updateNombreL(req: Request, res: Response): Promise<Response> {

        const ER_DUP_ENTRY = 'ER_DUP_ENTRY';

        const id = req.params.id;
        const body = req.body;

        const nombreLocal = await getRepository(NombreLocal).findOne(id);

        
        if( !nombreLocal ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un nombre local con ese id',
                errors: {message: 'No existe un nomber local con ese id.'}
            });
        }

        const nombreLocalAdd = new NombreLocal();

        nombreLocal.nombre_local = body.nombreLocal;
        nombreLocal.paises = body.paisId;
        nombreLocal.especie = body.especieId;

        const errors = await validate(nombreLocal);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al actualizar el nombre local',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                getRepository(NombreLocal).merge(nombreLocal, nombreLocalAdd);
                const datos = await getRepository(NombreLocal).save(nombreLocal);
                return res.status(200).json({
                    ok: true,
                    NombreLocal: datos
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
        
    }

    public async deleteNombreL(req: Request, res: Response): Promise<Response> {

        const id = req.params.id;
        const nombreLocal = await getRepository(NombreLocal).findOne(id);
        if( !nombreLocal ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un nombre local con id',
                errors: {message: 'No existe un nombre local con ese id.'}
            });
        }

        try {

            await getRepository(NombreLocal).delete(nombreLocal.id);  
            
            return res.status(200).json({
                ok: false,
                mensaje: 'El nombre comercial a sido eliminado',
                nombreLocal
            });

        } catch (error) {
            return res.status(400).json({
                ok: false,
                err: error
            });
        }
     
    }

}
