import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { User } from "../entity/user";
import { validate } from "class-validator";
import { resolve } from "path";
import { existsSync, unlink, exists } from "fs";
import { Especie } from "../entity/especie";
import multer from "multer";
import path from "path";
import { imageFilter } from "../libs/multer";
import jimp from "jimp";

export class UploadController {
  async updateFoto(req: any, res: Response) {
    const id = req.params.id;
    const tipo = req.params.tipo;

    const tiposValidos = ["especies", "usuarios"];

    const storageUser = multer.diskStorage({
      filename: (req, file, cb) => {
        const nombreCortado = file.originalname.split(".");
        const extensionArchivo = nombreCortado[nombreCortado.length - 1];
        const nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extensionArchivo}`;

        cb(null, nombreArchivo);
      },
      destination: (req, file, cb) => {
        cb(null, path.join(__dirname, "../../public/usuarios"));
      },
    });

    const storageEspecie = multer.diskStorage({
      filename: (req, file, cb) => {
        const nombreCortado = file.originalname.split(".");
        const extensionArchivo = nombreCortado[nombreCortado.length - 1];
        const nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extensionArchivo}`;

        cb(null, nombreArchivo);
      },
      destination: (req, file, cb) => {
        cb(null, path.join(__dirname, "../../public/especies"));
      },
    });

    const uploadUser = multer({
      dest: path.join(__dirname, "../../public"),
      storage: storageUser,
      fileFilter: imageFilter,
      limits: { fileSize: 10000000 },
    }).single("img");

    const uploadEsp = multer({
      dest: path.join(__dirname, "../../public"),
      storage: storageEspecie,
      fileFilter: imageFilter,
      limits: { fileSize: 10000000 },
    }).single("img");

    if (tiposValidos.indexOf(tipo) < 0) {
      return res.status(400).json({
        ok: false,
        mensaje: "Tipo de colecccion no es valido",
        errors: { message: "Tipo de colecccion no es valido" },
      });
    }

    if (tipo === "usuarios") {
      const user = await getRepository(User).findOne(id);

      if (!user) {
        return res.status(400).json({
          ok: false,
          mensaje: "Usuario no existe",
          errors: { message: "Usuario no existe." },
        });
      }

      uploadUser(req, res, async (err) => {
        if (err) {
          return res.json({
            ok: false,
            mesaje: err.message,
            error: err,
          });
        }

        if (!req.file) {
          return res.json({
            ok: false,
            menaje: "!Debe seleccionar una imagen¡",
            error: {
              message: "!Debe seleccionar una imagen¡",
              code: "NO_IMAGE",
            },
          });
        }

        const pathViejo = resolve(
          __dirname,
          `../../public/usuarios/${user.foto}`
        );
        const pathViejoResize = resolve(
          __dirname,
          `../../public/usuarios/${user.foto_resize}`
        );
        // console.log(pathViejo);
        if (existsSync(pathViejo) && existsSync(pathViejoResize)) {
          unlink(pathViejo, () => {});
          unlink(pathViejoResize, () => {});
        }

        user.foto = req.file.filename;

        const nombreCortado = req.file.filename.split(".");
        const extensionArchivo = nombreCortado[nombreCortado.length - 1];
        const nombreResize = `${nombreCortado[0]}-250x250.${extensionArchivo}`;

        const photo = await jimp.read(req.file.path);
        await photo.resize(250, jimp.AUTO);
        await photo.writeAsync(
          path.join(__dirname, `../../public/usuarios/${nombreResize}`)
        );

        user.foto_resize = nombreResize;

        try {
          await getRepository(User).update(id, {
            foto: user.foto,
            foto_resize: user.foto_resize,
          });
          return res.status(200).json({
            ok: true,
            foto: user.foto,
            mensaje: "La foto se ha actualizado corectamente",
            code: "SAVE_IMAGE",
          });
        } catch (error) {
          return res.status(400).json({
            ok: false,
            err: error,
          });
        }
      });
    }

    if (tipo == "especies") {
      const especie = await getRepository(Especie).findOne(id);

      if (!especie) {
        return res.status(400).json({
          ok: false,
          mensaje: "Usuario no existe",
          errors: { message: "Uusario no existe." },
        });
      }

      uploadEsp(req, res, async (err) => {
        if (err) {
          return res.json({
            ok: false,
            mesaje: err.message,
            error: err,
          });
        }

        if (!req.file) {
          return res.json({
            ok: false,
            menaje: "!Debe seleccionar una imagen¡",
            error: {
              message: "!Debe seleccionar una imagen¡",
              code: "NO_IMAGE",
            },
          });
        }

        const pathViejo = resolve(
          __dirname,
          `../../public/especies/${especie.img}`
        );
        const pathViejoResize = resolve(
          __dirname,
          `../../public/especies/${especie.img_resize}`
        );
        // console.log(pathViejo);
        if (existsSync(pathViejo) && existsSync(pathViejoResize)) {
          unlink(pathViejo, () => {});
          unlink(pathViejoResize, () => {});
        }

        especie.img = req.file.filename;

        const nombreCortado = req.file.filename.split(".");
        const extensionArchivo = nombreCortado[nombreCortado.length - 1];
        const nombreResize = `${nombreCortado[0]}-250x250.${extensionArchivo}`;

        // const photo = await jimp.read(req.file.path);
        // await photo.resize(250, jimp.AUTO);
        // await photo.writeAsync(
        //   path.join(__dirname, `../../public/especies/${nombreResize}`)
        // );

        especie.img_resize = nombreResize;

        try {
          await getRepository(Especie).update(id, {
            img: especie.img,
            img_resize: especie.img_resize,
          });
          return res.status(200).json({
            ok: true,
            foto: especie.img,
            foto_resize: especie.img_resize,
            mensaje: "La foto se ha actualizado corectamente",
            code: "SAVE_IMAGE",
          });
        } catch (error) {
          return res.status(400).json({
            ok: false,
            err: error,
          });
        }
      });
    }
  }

  async getFoto(req: any, res: Response) {
    var tipo = req.params.tipo;
    var img = req.params.img;

    let path = resolve(__dirname, `../../public/${tipo}/${img}`);

    exists(path, (existe) => {
      if (!existe) {
        path = resolve(__dirname, "../../assets/no-img.jpg");
      }
      res.sendFile(path);
    });
  }
}
