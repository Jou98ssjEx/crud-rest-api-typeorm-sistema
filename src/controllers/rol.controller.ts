import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { Rol } from './../entity/rol';

export class RolController {

    public async getRols(req: Request, res: Response): Promise<Response> {

        try {
            const rol = await getRepository(Rol).find();
            if( !rol ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'ERROR al buscar los roles'
                });
            }
            return res.json({
                ok: true,
                roles: rol
            });
        } catch (error) {
            return res.status(500).json({
                ok: true,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }

        
    }
    
    // public async getRol (req: Request, res: Response): Promise<Response> {
    //     const rolId = await getRepository(Rol).findOne(req.params.id); // es como hacer un select * from 
    //     return res.json(rolId);
    // }
    
    // public async createRols(req: Request, res: Response): Promise<Response> {
    //     // return res.json(req.body);
    //     const newUser = getRepository(Rol).create(req.body);
    //     const datosGuardados = await getRepository(Rol).save(newUser);
    //     return res.json(datosGuardados);
    // }
    
    // public async updateRol(req: Request, res: Response): Promise<Response> {
    //     const user = await getRepository(Rol).findOne(req.params.id);
    //     if (user){
    //         getRepository(Rol).merge(user, req.body);
    //         const datos = await getRepository(Rol).save(user);
    //         return res.json(datos);
    //     }
        
    //     return res.status(404).json({mensaje: ' No se encontró el rol del usuario'});
    // }
    
    // public async deleteRol(req: Request, res: Response): Promise<Response> {
    //     const rolId = await getRepository(Rol).delete(req.params.id); // es como hacer un select * from 
    //     return res.json(rolId);
    // }

}




