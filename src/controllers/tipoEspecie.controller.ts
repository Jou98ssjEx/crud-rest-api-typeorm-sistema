import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { validate } from 'class-validator';
import { TipoEspecie } from '../entity/tipo_especie';

export class TipoPescaController {

    public async getTipoEspecie(req: Request, res: Response): Promise<Response> {

        try {
            const especiePesca = await getRepository(TipoEspecie).find();
            if( !especiePesca ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'ERROR al obtener la especie de pesca'
                });
            }
            return res.json({
                ok: true,
                Especies: especiePesca
            });
        } catch (error) {
            return res.status(500).json({
                ok: true,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
        
    }
    
    public async getTipoEspecieId(req: Request, res: Response): Promise<Response> {
    
        const id = req.params.id;
        try {
            const especiePescaId = await getRepository(TipoEspecie).findOne(id); 

            if( !especiePescaId ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'No existe una especie de pesca con ese id',
                    errors: {message: 'No existe una especie de pesca con ese id.'}
                });
            }

            return res.status(200).json({
                ok: true,
                Especie: especiePescaId
            });

        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
    }
    
    public async createTipoEspecie(req: Request, res: Response): Promise<Response> {

        const body = req.body;
    
        const especie = new TipoEspecie();
        
        especie.tipo = body.nombre;
    
        const errors = await validate(especie);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al crear la especie de pesca',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                const datosGuardados = await getRepository(TipoEspecie).save(especie);
                return res.status(201).json({
                        ok: true,
                        especiePesca: datosGuardados
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
    }

    public async updateTipoEspecie(req: Request, res: Response): Promise<Response> {

        const id = req.params.id;
        const body = req.body;

        const especie = await getRepository(TipoEspecie).findOne(id);

        if( !especie ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe una especie de pesca con ese id',
                errors: {message: 'No existe una especie de pesca con ese id.'}
            });
        }

        const especieAdd = new TipoEspecie();
        especie.tipo = body.nombre;

        const errors = await validate(especie);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al actualizar la especie de pesca',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                getRepository(TipoEspecie).merge(especie, especieAdd);
                const datos = await getRepository(TipoEspecie).save(especie);
                return res.status(200).json({
                    ok: true,
                    TipoEspecie: datos
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
        
    }

    public async deleteTipoEspecie(req: Request, res: Response): Promise<Response> {

        const ER_ROW_IS_REFERENCED_2 = 'ER_ROW_IS_REFERENCED_2';
        const id = req.params.id;
        const especie = await getRepository(TipoEspecie).findOne(id);
        if( !especie ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe una especie con id',
                errors: {message: 'No existe una especie con ese id.'}
            });
        }

        try {

            await getRepository(TipoEspecie).delete(especie.id);  
            
            return res.status(200).json({
                ok: false,
                mensaje: 'La especie de pesca a sido eliminado',
                especie     
            });

        } catch (error) {

            if(error.code === ER_ROW_IS_REFERENCED_2 ) {
                return res.status(400).json({
                        ok: false,
                        mensaje: 'ERROR el tipo de especie esta vinculado con otra tabla',
                        error: 'No se puede eliminar o actualizar una fila principal: falla una restricción de clave externa',
                        err: error
                });
            }

            return res.status(400).json({
                ok: false,
                err: error
            });
        }
     
    }
}