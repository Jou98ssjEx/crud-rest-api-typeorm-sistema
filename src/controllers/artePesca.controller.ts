import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { validate } from 'class-validator';
import { ArtePesca } from '../entity/arte_pesca';

export class ArtePescaController {

    public async getArtePesca(req: Request, res: Response): Promise<Response> {

        try {
            const artePesca = await getRepository(ArtePesca).find();
            if( !artePesca ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'ERROR al obtener el arte de pesca'
                });
            }
            return res.json({
                ok: true,
                ArtePesca: artePesca
            });
        } catch (error) {
            return res.status(500).json({
                ok: true,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
        
    }
    
    public async getArtePescaId(req: Request, res: Response): Promise<Response> {
    
        try {
            const artePescaId = await getRepository(ArtePesca).findOne(req.params.id); 

            if( !artePescaId ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'No existe un arte de pesca con ese id',
                    errors: {message: 'No existe un arte de pesca con ese id.'}
                });
            }

            return res.status(200).json({
                ok: true,
                ArtePesca: artePescaId
            });

        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
    }
    
    public async createArtePesca(req: Request, res: Response): Promise<Response> {

        const body = req.body;
    
        const arte = new ArtePesca();
        
        arte.nombre = body.nombre;
    
        const errors = await validate(arte);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al crear el arte de pesca',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                const datosGuardados = await getRepository(ArtePesca).save(arte);
                return res.status(201).json({
                        ok: true,
                        ArtePesca: datosGuardados
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
    }

    public async updateArtePesca(req: Request, res: Response): Promise<Response> {

        const id = req.params.id;
        const body = req.body;

        const arte = await getRepository(ArtePesca).findOne(id);

        if( !arte ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un arte de pesca con ese id',
                errors: {message: 'No existe un arte de pesca con ese id.'}
            });
        }

        const arteAdd = new ArtePesca();
        arte.nombre = body.nombre;

        const errors = await validate(arte);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al actualizar el arte de pesca',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                getRepository(ArtePesca).merge(arte, arteAdd);
                const datos = await getRepository(ArtePesca).save(arte);
                return res.status(200).json({
                    ok: true,
                    ArtePesca: datos
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
        
    }

    public async deleteArtePesca(req: Request, res: Response): Promise<Response> {
        const ER_ROW_IS_REFERENCED_2 = 'ER_ROW_IS_REFERENCED_2';
        const id = req.params.id;
        const arte = await getRepository(ArtePesca).findOne(id);
        if( !arte ){
            return res.status(400).json({
                ok: true,
                mensaje: 'No existe el arte de pesca con este id',
                errors: {message: 'No existe el arte de pesca con este id.'}
            });
        }

        try {

            await getRepository(ArtePesca).delete(arte.id);  
            
            return res.status(200).json({
                ok: false,
                mensaje: 'El arte de pesca a sido eliminado',
                arte     
            });

        } catch (error) {
            if(error.code === ER_ROW_IS_REFERENCED_2 ) {
                return res.status(400).json({
                        ok: false,
                        mensaje: 'ERROR el tipo de pesca esta vinculado con otra tabla',
                        error: 'No se puede eliminar o actualizar una fila principal: falla una restricción de clave externa',
                        err: error
                });
            }
            return res.status(400).json({
                ok: false,
                err: error
            });
        }
     
    }
}