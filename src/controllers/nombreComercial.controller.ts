import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { NombreComercial } from '../entity/nombre_comercial';
import { validate } from 'class-validator';

export class NombreComercialController {
    
    public async getNombresComerciales(req: Request, res: Response): Promise<Response> {

        try {
            const nombresC = await getRepository(NombreComercial).find({ relations: ["especie"], 
            select: ['nombre', 'detalle', 'id'] });

            if( !nombresC ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'ERROR al buscar los nombres comerciales existentes'
                });
            }

            return res.status(200).json({
                ok: true,
                nombresComerciales: nombresC
            });
        } catch (error) {
            return res.status(500).json({
                ok: true,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
        
    }

    public async getNombreComercial(req: Request, res: Response): Promise<Response> {
    
        try {
            const nombreCoId = await getRepository(NombreComercial).findOne(req.params.id, { relations: ["especie"],
            select: ['nombre', 'detalle', 'id'] }); 

            if( !nombreCoId ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'No existe un nombre comercial con ese id',
                    errors: {message: 'No existe un nombre comercial con ese id.'}
                });
            }

            return res.status(200).json({
                ok: true,
                NombreComercial: nombreCoId
            });

        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
        
        
    }

    public async createNombreC(req: Request, res: Response): Promise<Response> {

        const ER_DUP_ENTRY = 'ER_DUP_ENTRY';

        const body = req.body;
    
        const nombreComercial = new NombreComercial();
        nombreComercial.nombre = body.nombre;
        nombreComercial.detalle = body.detalle;
        nombreComercial.especie = body.especie;
    
        const errors = await validate(nombreComercial);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al crear nombre comercial',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                const datosGuardados = await getRepository(NombreComercial).save(nombreComercial);
                return res.status(201).json({
                        ok: true,
                        NombreComercial: datosGuardados
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
    }

    public async updateNombreC(req: Request, res: Response): Promise<Response> {

        const ER_DUP_ENTRY = 'ER_DUP_ENTRY';

        const id = req.params.id;
        const body = req.body;

        const nombreComercial = await getRepository(NombreComercial).findOne(id);

        
        if( !nombreComercial ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un nombre comercial con ese id',
                errors: {message: 'No existe un nomber comercial con ese id.'}
            });
        }

        const nombreComercialAdd = new NombreComercial();

        nombreComercial.nombre = body.nombre;
        nombreComercial.detalle = body.detalle;
        nombreComercial.especie = body.especie;

        const errors = await validate(nombreComercial);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al actualizar el nombre comercial',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                getRepository(NombreComercial).merge(nombreComercial, nombreComercialAdd);
                const datos = await getRepository(NombreComercial).save(nombreComercial);
                return res.status(200).json({
                    ok: true,
                    NombreComercial: datos
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
        
    }

    public async deleteNombreC(req: Request, res: Response): Promise<Response> {

        const id = req.params.id;
        const nombreComercial = await getRepository(NombreComercial).findOne(id);
        if( !nombreComercial ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un usuario con id',
                errors: {message: 'No existe un usuario con ese id.'}
            });
        }

        try {

            await getRepository(NombreComercial).delete(nombreComercial.id);  
            
            return res.status(200).json({
                ok: false,
                mensaje: 'El nombre comercial a sido eliminado',
                nombreComercial
            });

        } catch (error) {
            return res.status(400).json({
                ok: false,
                err: error
            });
        }
     
    }

}
