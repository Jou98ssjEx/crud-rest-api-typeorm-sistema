import { Request, Response } from "express";
import { getRepository } from "typeorm";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { User } from "../entity/user";
import { validate } from "class-validator";
import { getConnection } from "typeorm";

export class UsuarioController {
  public async getUsers(req: Request, res: Response): Promise<Response> {
    try {
      // const usuarios = await getRepository(User).find({
      //   relations: ["rol"],
      //   select: [
      //     "nombre_usuario",
      //     "apellido_usuario",
      //     "email_usuario",
      //     "foto",
      //     "foto_resize",
      //     "id",
      //     "user_name",
      //   ],
      // });

      const usuarios = await getRepository(User).find({relations:  ["rol"],}) ;

      if (!usuarios) {
        return res.status(400).json({
          ok: false,
          mensaje: "ERROR al buscar los usuarios",
        });
      }

      return res.status(200).json({
        ok: true,
        usuario: usuarios,
      });
    } catch (error) {
      return res.status(500).json({
        ok: true,
        mensaje: "ERROR interno del servidor",
        error,
      });
    }
  }


  public async getUser(req: Request, res: Response): Promise<Response> {
    const id = req.params.id;
    try {
      // const usuariosId = await getRepository(User).findOne(id , {
      //   relations: ["rol"],
      //   select: [
      //     "nombre_usuario",
      //     "apellido_usuario",
      //     "email_usuario",
      //     "foto",
      //     "foto_resize",
      //     "id",
      //     "user_name",
      //   ],
      // });

      const usuariosId = await getRepository(User).findOne( id, {relations:["rol"], }) ;
      // const usuariosId = await getRepository(User).find({
      //   where: {
      //     id,
      //   },
      //   join: {
      //     alias: "user",
      //     // innerJoinAndSelect: {
      //     //   familia: "especie.familia",
      //     //   artePesca: "especie.artePesca",
      //     //   tipoPesca: "especie.tipoPesca",
      //     //   tipoEspecie: "especie.tipoEspecie",
      //     //   medida: "especie.medida"  // agg tabla medida

      //     // },
      //     leftJoinAndSelect: {
      //       rol: "user.rolId"
      //       // foto: "especie.fotos",
      //       // nombreComercial: "especie.nombreComercial",
      //       // nombreLocal: "especie.nombreLocal",
      //       // paises: "nombreLocal.paises",
      //       // veda: "especie.veda",
      //       // location: "especie.location",
      //     },
      //   },
      // });


      if (!usuariosId) {
        return res.status(400).json({
          ok: false,
          mensaje: "No existe un usuario con ese id",
          errors: { message: "No existe un usuario con ese id." },
        });
      }

      return res.status(200).json({
        ok: true,
        usuario: usuariosId,
      });
    } catch (error) {
      return res.status(500).json({
        ok: false,
        mensaje: "ERROR interno del servidor",
        error,
      });
    }
  }

  public async createUsers(req: Request, res: Response): Promise<Response> {
    const ER_DUP_ENTRY = "ER_DUP_ENTRY";

    const body = req.body;

    const user = new User();
    user.nombre_usuario = body.nombre_usuario;
    user.apellido_usuario = body.apellido_usuario;
    user.email_usuario = body.email_usuario;
    user.pass_usuario = bcrypt.hashSync(body.pass_usuario, 10);
    user.user_name = body.user_name;
    user.foto = body.foto;
    user.rol = body.rol;

    const errors = await validate(user);

    if (errors.length > 0) {
      return res.status(400).json({
        ok: false,
        mensaje: "ERROR al crear usuario",
        mesajeError: errors[0].constraints.isNotEmpty,
        errors,
      });
    } else {
      try {
        const datosGuardados = await getRepository(User).save(user);
        datosGuardados.pass_usuario = ":)";
        return res.status(201).json({
          ok: true,
          usuario: datosGuardados,
        });
      } catch (error) {
        if (error.code === ER_DUP_ENTRY) {
          return res.status(400).json({
            ok: false,
            mensaje: "ERROR el email debe ser unico",
            err: error,
          });
        }

        return res.status(400).json({
          ok: false,
          err: error,
        });
      }
    }
  }

  public async updateUser(req: Request, res: Response): Promise<Response> {
    const ER_DUP_ENTRY = "ER_DUP_ENTRY";

    const id = req.params.id;
    const body = req.body;

    const user = await getRepository(User).findOne(id);

    if (!user) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe un usuario con ese id",
        errors: { message: "No existe un usuario con ese id." },
      });
    }

    const userAdd = new User();
    user.nombre_usuario = body.nombre_usuario;
    user.apellido_usuario = body.apellido_usuario;
    user.email_usuario = body.email_usuario;
    user.user_name = body.user_name;
    user.rol = body.rol;

    const errors = await validate(user);

    if (errors.length > 0) {
      return res.status(400).json({
        ok: false,
        mensaje: "ERROR al actualizar el usuario",
        mesajeError: errors[0].constraints.isNotEmpty,
        errors,
      });
    } else {
      try {
        getRepository(User).merge(user, userAdd);
        const datos = await getRepository(User).save(user);
        datos.pass_usuario = ":)";
        return res.status(200).json({
          ok: true,
          usuario: datos,
        });
      } catch (error) {
        if (error.code === ER_DUP_ENTRY) {
          return res.status(400).json({
            ok: false,
            mensaje: "ERROR el email debe ser unico",
            err: error,
          });
        }

        return res.status(400).json({
          ok: false,
          err: error,
        });
      }
    }
  }

  public async deleteUser(req: Request, res: Response): Promise<Response> {
    const id = req.params.id;
    const user = await getRepository(User).findOne(id);
    if (!user) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe un usuario con id",
        errors: { message: "No existe un usuario con ese id." },
      });
    }

    try {
      await getRepository(User).delete(user.id);

      return res.status(200).json({
        ok: true,
        mensaje: "El usuario a sido eliminado",
        user,
      });
    } catch (error) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe un usuario con id",
        err: error,
      });
    }
  }

  public async updatePass(req: Request, res: Response): Promise<Response> {
    const pass = req.body;
    const id = req.params.id;

    const user: any = await getRepository(User).findOne(id);

    if (!user) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe un usuario con ese id",
        errors: { message: "No existe un usuario con ese id." },
      });
    }

    // Confirmar si las contraseñas nuevas son validadas
    if (pass.pass_nue !== pass.pass_nue_rep) {
      return res.status(400).json({
        ok: false,
        mensaje: "Las contraseñas no coinciden",
        errors: { message: "Las contraseñas no coinciden." },
      });
    }

    try {
      if (bcrypt.compareSync(pass.pass_ant, user.pass_usuario)) {
        await getConnection()
          .createQueryBuilder()
          .update(User)
          .set({
            pass_usuario: bcrypt.hashSync(pass.pass_nue, 10),
          })
          .where("id = :id", { id })
          .execute();

        return res.status(200).json({
          ok: true,
          mensaje: "Contraseña actualizada corectamente.",
        });
      }

      return res.status(400).json({
        ok: false,
        mensaje: "Contraseña antigua incorecta",
      });
    } catch (error) {
      return res.status(500).json({
        ok: false,
        mensaje: error,
      });
    }
  }
}
