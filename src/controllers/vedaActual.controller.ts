import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Veda } from "./../entity/veda";
import moment from "moment";

export class VedaActualController {
  // Metodo para obtener solo las vedas que esten activas
  public async getVeda(req: Request, res: Response): Promise<Response> {
    try {
      //& Consultamos las base de datos y traemos todas las vedas
      const veda = await getRepository(Veda).find({
        relations: ["especie"],
      });

      // Verificamos si existe un error al obtener las vedas
      if (!veda) {
        return res.status(400).json({
          ok: false,
          mensaje: "ERROR al obtener la veda",
        });
      }

      // Mapeamos las vedas para verificar si estan activas
      var start = moment(moment(), "YYYY-MM-DD");
      const vedas = veda.map((d) => {
        var end = moment(d.inicio, "YYYY-MM-DD");
        const estaVeda = moment(start).isBetween(d.inicio, d.fin);
        const estaCerca = moment.duration(end.diff(start)).asDays();
        if (estaCerca > 0 && estaCerca < 5) {
          d.isVeda = true;
        }
        if (estaVeda) {
          d.isVeda = true;
        }
        return d;
      });
      // filtramos solo las vedas que este true
      let resultado = vedas.filter((v) => v.isVeda === true);
      // en caso de que no exista una veda activa se devuleve un objeto vacio
      if (resultado.length < 1) {
        return res.status(200).json({
          ok: true,
          vedaActual: {
            inicio: new Date("0000-00-00"),
            fin: new Date("0000-00-00"),
            isVeda: false,
          },
        });
      }
      // en caso que no pase nade en los procesos anteriores
      // se devuelve las vedas activas
      return res.json({
        ok: true,
        vedaActual: resultado,
      });
    } catch (error) {
      return res.status(500).json({
        ok: true,
        mensaje: "ERROR interno del servidor",
        error,
      });
    }
  }
}
