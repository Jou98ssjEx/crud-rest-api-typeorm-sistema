import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate, Length } from "class-validator";
import { resolve } from "path";
import {
  existsSync,
  unlink,
  exists,
  readFile,
  writeFile,
  mkdir,
  mkdirSync,
  symlinkSync,
} from "fs";
import { Foto } from "../entity/foto";
import multer from "multer";
import path from "path";
import { imageFilter } from "../libs/multer";
import { v4 as uuidv4 } from "uuid";
import jimp from "jimp";

export class FotoController {
  async getFotos(req: any, res: Response) {
    try {
      const especie = await getRepository(Foto).find();
      if (!especie) {
        return res.status(400).json({
          ok: false,
          mensaje: "ERROR al buscar las imagenes",
        });
      }
      return res.json({
        ok: true,
        FotosDB: especie,
      });
    } catch (error) {
      return res.status(500).json({
        ok: true,
        mensaje: "ERROR interno del servidor",
        error,
      });
    }
  }

  async saveFoto(req: any, res: any) {
    const id = req.params.id;
    const tipo = req.params.tipo;

    const storageFoto = multer.diskStorage({
      filename: (req, file, cb) => {
        const nombreCortado = file.originalname.split(".");
        const extensionArchivo = nombreCortado[nombreCortado.length - 1];
        const nombreArchivo = `${uuidv4()}-${new Date().getMilliseconds()}.${extensionArchivo}`;
        console.log("Nombre y extension:  " + nombreArchivo);

        cb(null, nombreArchivo);
      },
      destination: (req, file, cb) => {
        const dest = path.join(__dirname, `../../public/foto/${tipo}`);
        mkdirSync(dest, { recursive: true });
        return cb(null, dest);
      },
    });

    const uploadFoto = multer({
      storage: storageFoto,
      fileFilter: imageFilter,
      limits: { fileSize: 10000000 },
    }).array("img", 10);

    uploadFoto(req, res, async (err) => {
      if (err) {
        return res.json({
          ok: false,
          mesaje: err.message,
          error: err,
        });
      }

      if (req.files < 1) {
        return res.json({
          ok: false,
          menaje: "!Debe seleccionar una imagen¡",
          error: {
            message: "!Debe seleccionar una imagen¡",
            code: "NO_IMAGE",
          },
        });
      }

      for (let i = 0; i < req.files.length; i++) {
        // Se renombra la imagen para poder diferenciar entre una imagen
        // redimencionada y no. Se le aumenta antes del .(extencion)
        // un 250x250
        const nombreCortado = req.files[i].filename.split(".");
        const extensionArchivo = nombreCortado[nombreCortado.length - 1];
        const nombreResize = `${nombreCortado[0]}-250x250.${extensionArchivo}`;

        // Utilizamos la libreria JIMP para redimensionar la imagen
        // como esta dentro del FOR una a una se ma redimensionando
        // y ubicando en la misma carpeta con su respectivo nombre
        // ejemplo:
        // imagen original: 1be7660e-6be5-4f2f-998e-168778bf88b0-61.jpg
        // imagen redimencinada: 1be7660e-6be5-4f2f-998e-168778bf88b0-61-250x250.jpg
        // const photo = await jimp.read(req.files[i].path);
        // await photo.resize(250, jimp.AUTO);
        // await photo.writeAsync(
        //   path.join(__dirname, `../../public/foto/${tipo}/${nombreResize}`)
        // );

        // Se le aumento un nuevo campo a la tabla foto
        // donde llevara el nombre de la foto redimencionada
        const foto = new Foto();
        foto.archivo = req.files[i].filename;
        foto.especie = id;
        foto.resize = nombreResize;

        try {
          await getRepository(Foto).save(foto);
        } catch (error) {
          return res.status(400).json({
            ok: false,
            err: error,
          });
        }
      }

      return res.status(200).json({
        ok: true,
        mensaje: "La foto se ha actualizado corectamente",
        code: "SAVE_IMAGE",
      });
    });
  }

  async getFoto(req: any, res: Response) {
    var tipo = req.params.tipo;
    var img = req.params.img;

    let path = resolve(__dirname, `../../public/foto/${tipo}/${img}`);

    exists(path, (existe) => {
      if (!existe) {
        path = resolve(__dirname, "../../assets/no-img.jpg");
      }
      res.sendFile(path);
    });
  }

  public async deleteFoto(req: Request, res: Response): Promise<Response> {
    const id = req.params.id;
    // const archivo = req.params.archivo;

    const especie = await getRepository(Foto).findOne(id);
    // const data = await getRepository(Foto).findOne(archivo);

    if (!especie) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe una imagen con id",
        errors: { message: "No existe una imagen con ese id." },
      });
    }

    try {
      // Eliminar por id
      await getRepository(Foto).delete(especie.id);

      return res.status(200).json({
        ok: true,
        mensaje: "La imagen del pez a sido eliminado",
        especie,
      });
    } catch (error) {
      // if(error.code === ER_ROW_IS_REFERENCED_2 ) {
      //     return res.status(400).json({
      //             ok: false,
      //             mensaje: 'ERROR el tipo de especie esta vinculado con otra tabla',
      //             error: 'No se puede eliminar o actualizar una fila principal: falla una restricción de clave externa',
      //             err: error
      //     });
      // }

      return res.status(400).json({
        ok: false,
        err: error,
      });
    }
  }

  // Sin funcionamineto
  async delelteStorageFoto(req: any, res: Response) {
    // parametros para eliminar la img
    var tipo = req.params.tipo;
    var img = req.params.img;

    let path = resolve(__dirname, `../../public/foto/${tipo}/${img}`);

    exists(path, (existe) => {
      if (!existe) {
        path = resolve(__dirname, "../../assets/no-img.jpg");
      }
      let archivo = res.sendFile(path);
      // deleteFile(archivo);
    });

    try {
    } catch (e) {}
  }
}
