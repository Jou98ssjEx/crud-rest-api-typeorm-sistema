import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { validate } from 'class-validator';
import { Location } from '../entity/location';

export class LocationController {

    public async getLocation(req: Request, res: Response): Promise<Response> {

        try {
            const localidad = await getRepository(Location).find({ relations: ["especie"],});
            if( !localidad ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'ERROR al buscar las localidades'
                });
            }
            return res.json({
                ok: true,
                localidades: localidad
            });
        } catch (error) {
            return res.status(500).json({
                ok: true,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
 
    }

    public async getLocationId(req: Request, res: Response): Promise<Response> {
    
        const id = req.params.id;
        try {
            const locationId = await getRepository(Location).findOne({
                where: {
                  id,
                },
                join: {
                  alias: "location",
                  leftJoinAndSelect: {
                    especie: "location.especie",
                  },
                },
              });

            if( !locationId ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'No existe una localidad con ese id',
                    errors: {message: 'No existe una localidad con ese id.'}
                });
            }

            return res.status(200).json({
                ok: true,
                Location: locationId
            });

        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
    }
    
    public async createLocation(req: Request, res: Response): Promise<Response> {

        const body = req.body;
    
        const localidad = new Location();
        
        localidad.localidad = body.localidad;
        localidad.latitud = body.latitud;
        localidad.longitud = body.longitud;
        localidad.especie = body.especieId;

    
        const errors = await validate(localidad);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al crear la localidad',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                const datosGuardados = await getRepository(Location).save(localidad);
                return res.status(201).json({
                        ok: true,
                        Location: datosGuardados
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
    }

    public async updateLocation(req: Request, res: Response): Promise<Response> {

        const id = req.params.id;
        const body = req.body;

        const localidad = await getRepository(Location).findOne(id);

        if( !localidad ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe una localidad con ese id',
                errors: {message: 'No existe una localidad con ese id.'}
            });
        }

        const locationAdd = new Location();
        localidad.localidad = body.localidad;
        localidad.latitud = body.latitud;
        localidad.longitud = body.longitud;
        localidad.especie = body.especieId;


        const errors = await validate(localidad);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al actualizar la localidad',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                getRepository(Location).merge(localidad, locationAdd);
                const datos = await getRepository(Location).save(localidad);
                return res.status(200).json({
                    ok: true,
                    localidad: datos
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
        
    }

    public async deleteLocation(req: Request, res: Response): Promise<Response> {
        const ER_ROW_IS_REFERENCED_2 = 'ER_ROW_IS_REFERENCED_2';
        const id = req.params.id;
        const localidad = await getRepository(Location).findOne(id);
        if( !localidad ){
            return res.status(400).json({
                ok: true,
                mensaje: 'No existe la localidad con este id',
                errors: {message: 'No existe la localidad con este id.'}
            });
        }

        try {

            await getRepository(Location).delete(localidad.id);  
            
            return res.status(200).json({
                ok: false,
                mensaje: 'La localidad a sido eliminada',
                localidad     
            });

        } catch (error) {
            if(error.code === ER_ROW_IS_REFERENCED_2 ) {
                return res.status(400).json({
                        ok: false,
                        mensaje: 'ERROR el tipo de localidad esta vinculado con otra tabla',
                        error: 'No se puede eliminar o actualizar una fila principal: falla una restricción de clave externa',
                        err: error
                });
            }
            return res.status(400).json({
                ok: false,
                err: error
            });
        }
     
    }

}