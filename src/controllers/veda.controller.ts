import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Veda } from "./../entity/veda";
import { PaisRoutes } from "../routes/pais.routes";
import { validate } from "class-validator";
import moment from "moment";

export class VedaController {
  public async getVeda(req: Request, res: Response): Promise<Response> {
    try {
      const veda = await getRepository(Veda).find({
        relations: ["especie"],
      });
      if (!veda) {
        return res.status(400).json({
          ok: false,
          mensaje: "ERROR al obtener la veda",
        });
      }
      return res.json({
        ok: true,
        veda,
      });
    } catch (error) {
      return res.status(500).json({
        ok: true,
        mensaje: "ERROR interno del servidor",
        error,
      });
    }
  }

  public async getVedaId(req: Request, res: Response): Promise<Response> {
    const id = req.params.id;

    try {
      const vedaId = await getRepository(Veda).findOne(id);

      if (!vedaId) {
        return res.status(400).json({
          ok: false,
          mensaje: "No existe una veda con ese id",
          errors: { message: "No existe una veda con ese id." },
        });
      }

      return res.status(200).json({
        ok: true,
        veda: vedaId,
      });
    } catch (error) {
      return res.status(500).json({
        ok: false,
        mensaje: "ERROR interno del servidor",
        error,
      });
    }
  }

  public async createVeda(req: Request, res: Response): Promise<Response> {
    const body = req.body;

    const veda = new Veda();

    veda.inicio = body.inicio;
    veda.fin = body.fin;
    veda.especie = body.especieId;

    const errors = await validate(veda);

    if (errors.length > 0) {
      return res.status(400).json({
        ok: false,
        mensaje: "ERROR al crear veda",
        mesajeError: errors[0].constraints.isNotEmpty,
        errors,
      });
    } else {
      try {
        const datosGuardados = await getRepository(Veda).save(veda);
        return res.status(201).json({
          ok: true,
          veda: datosGuardados,
        });
      } catch (error) {
        return res.status(400).json({
          ok: false,
          err: error,
        });
      }
    }
  }

  public async updateVeda(req: Request, res: Response): Promise<Response> {
    const id = req.params.id;
    const body = req.body;

    const veda = await getRepository(Veda).findOne(id);

    if (!veda) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe una veda con ese id",
        errors: { message: "No existe una veda con ese id." },
      });
    }

    const vedaAdd = new Veda();
    veda.inicio = body.inicio;
    veda.fin = body.fin;
    veda.especie = body.especieId;

    const errors = await validate(veda);

    if (errors.length > 0) {
      return res.status(400).json({
        ok: false,
        mensaje: "ERROR al actualizar la veda",
        mesajeError: errors[0].constraints.isNotEmpty,
        errors,
      });
    } else {
      try {
        getRepository(Veda).merge(veda, vedaAdd);
        const datos = await getRepository(Veda).save(veda);
        return res.status(200).json({
          ok: true,
          veda: datos,
        });
      } catch (error) {
        return res.status(400).json({
          ok: false,
          err: error,
        });
      }
    }
  }

  public async deleteVeda(req: Request, res: Response): Promise<Response> {
    const id = req.params.id;
    const veda = await getRepository(Veda).findOne(id);
    if (!veda) {
      return res.status(400).json({
        ok: false,
        mensaje: "No existe una veda con id",
        errors: { message: "No existe una veda con ese id." },
      });
    }

    try {
      await getRepository(Veda).delete(veda.id);

      return res.status(200).json({
        ok: true,
        mensaje: "La Veda a sido eliminado",
        veda,
      });
    } catch (error) {
      return res.status(400).json({
        ok: false,
        err: error,
      });
    }
  }
}
