import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { Paises } from './../entity/pais';
import { PaisRoutes } from '../routes/pais.routes';
import { validate } from 'class-validator';

export class PaisController {

    public async getPais(req: Request, res: Response): Promise<Response> {

        try {
            const pais = await getRepository(Paises).find();
            if( !pais ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'ERROR al obtener pais'
                });
            }
            return res.json({
                ok: true,
                paises: pais
            });
        } catch (error) {
            return res.status(500).json({
                ok: true,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
        
    }
    
    public async getPaisId(req: Request, res: Response): Promise<Response> {
    
        const id = req.params.id;

        try {
            const paisId = await getRepository(Paises).findOne(id); 

            if( !paisId ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'No existe un país con ese id',
                    errors: {message: 'No existe un país con ese id.'}
                });
            }

            return res.status(200).json({
                ok: true,
                pais: paisId
            });

        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
    }
    
    public async createPais(req: Request, res: Response): Promise<Response> {

        const body = req.body;
    
        const pais = new Paises();
        
        pais.nombre = body.nombre;
        pais.iso = body.iso;
    
        const errors = await validate(pais);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al crear pais',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                const datosGuardados = await getRepository(Paises).save(pais);
                return res.status(201).json({
                        ok: true,
                        pais: datosGuardados
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
    }

    public async updatePais(req: Request, res: Response): Promise<Response> {

        const id = req.params.id;
        const body = req.body;

        const pais = await getRepository(Paises).findOne(id);

        if( !pais ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un pais con ese id',
                errors: {message: 'No existe un pais con ese id.'}
            });
        }

        const paisAdd = new Paises();
        pais.nombre = body.nombre;

        const errors = await validate(pais);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al actualizar el pais',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                getRepository(Paises).merge(pais, paisAdd);
                const datos = await getRepository(Paises).save(pais);
                return res.status(200).json({
                    ok: true,
                    paises: datos
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
        
    }

    public async deletePais(req: Request, res: Response): Promise<Response> {

        const id = req.params.id;
        const pais = await getRepository(Paises).findOne(id);
        if( !pais ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un pais con id',
                errors: {message: 'No existe un pais con ese id.'}
            });
        }

        try {

            await getRepository(Paises).delete(pais.id);  
            
            return res.status(200).json({
                ok: true,
                mensaje: 'El País a sido eliminado',
                pais
            });

        } catch (error) {
            return res.status(400).json({
                ok: false,
                err: error
            });
        }
     
    }
}    