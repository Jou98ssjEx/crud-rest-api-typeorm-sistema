import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { validate } from 'class-validator';
import { Medida } from '../entity/medidas';

export class MedidaController {

    public async getMedidas(req: Request, res: Response): Promise<Response> {

        try {
            const medida = await getRepository(Medida).find();
            if( !medida ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'ERROR al buscar las medidas'
                });
            }
            return res.json({
                ok: true,
                medidas: medida
            });
        } catch (error) {
            return res.status(500).json({
                ok: true,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
 
    }

    public async getMedidaId(req: Request, res: Response): Promise<Response> {
    
        try {
            const medidaId = await getRepository(Medida).findOne(req.params.id); 

            if( !medidaId ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'No existe una medida con ese id',
                    errors: {message: 'No existe una medida con ese id.'}
                });
            }

            return res.status(200).json({
                ok: true,
                Medida: medidaId
            });

        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
    }
    
    public async createMedida(req: Request, res: Response): Promise<Response> {

        const body = req.body;
    
        const medida = new Medida();
        
        medida.unidad = body.unidad;
    
        const errors = await validate(medida);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al crear la medida',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                const datosGuardados = await getRepository(Medida).save(medida);
                return res.status(201).json({
                        ok: true,
                        Medida: datosGuardados
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
    }

    public async updateMedida(req: Request, res: Response): Promise<Response> {

        const id = req.params.id;
        const body = req.body;

        const medida = await getRepository(Medida).findOne(id);

        if( !medida ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe una medida con ese id',
                errors: {message: 'No existe una medida con ese id.'}
            });
        }

        const medidaAdd = new Medida();
        medida.unidad = body.unidad;

        const errors = await validate(medida);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al actualizar la medida',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                getRepository(Medida).merge(medida, medidaAdd);
                const datos = await getRepository(Medida).save(medida);
                return res.status(200).json({
                    ok: true,
                    medida: datos
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
        
    }

    public async deleteMedida(req: Request, res: Response): Promise<Response> {
        const ER_ROW_IS_REFERENCED_2 = 'ER_ROW_IS_REFERENCED_2';
        const id = req.params.id;
        const medida = await getRepository(Medida).findOne(id);
        if( !medida ){
            return res.status(400).json({
                ok: true,
                mensaje: 'No existe la medida con este id',
                errors: {message: 'No existe la medida con este id.'}
            });
        }

        try {

            await getRepository(Medida).delete(medida.id);  
            
            return res.status(200).json({
                ok: false,
                mensaje: 'La medida a sido eliminada',
                medida     
            });

        } catch (error) {
            if(error.code === ER_ROW_IS_REFERENCED_2 ) {
                return res.status(400).json({
                        ok: false,
                        mensaje: 'ERROR el tipo de medida esta vinculado con otra tabla',
                        error: 'No se puede eliminar o actualizar una fila principal: falla una restricción de clave externa',
                        err: error
                });
            }
            return res.status(400).json({
                ok: false,
                err: error
            });
        }
     
    }

}