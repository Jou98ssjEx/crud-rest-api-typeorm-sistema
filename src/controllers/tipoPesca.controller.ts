import { Request, Response } from 'express';
import { getRepository } from 'typeorm';
import { validate } from 'class-validator';
import { TipoPesca } from '../entity/tipo_pesca';

export class TipoPescaController {

    public async getTipoPesca(req: Request, res: Response): Promise<Response> {

        try {
            const tipoPesca = await getRepository(TipoPesca).find();
            if( !tipoPesca ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'ERROR al obtener el tipo de pesca'
                });
            }
            return res.json({
                ok: true,
                TipoPesca: tipoPesca
            });
        } catch (error) {
            return res.status(500).json({
                ok: true,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
        
    }
    
    public async getTipoPescaId(req: Request, res: Response): Promise<Response> {
    
        const id = req.params.id;

        try {
            const tipoPescaId = await getRepository(TipoPesca).findOne(id); 

            if( !tipoPescaId ){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'No existe un tipo de pesca con ese id',
                    errors: {message: 'No existe un tipo de pesca con ese id.'}
                });
            }

            return res.status(200).json({
                ok: true,
                TipoPesca: tipoPescaId
            });

        } catch (error) {
            return res.status(500).json({
                ok: false,
                mensaje: 'ERROR interno del servidor',
                error
            });
        }
    }
    
    public async createTipoPesca(req: Request, res: Response): Promise<Response> {

        const body = req.body;
    
        const tipo = new TipoPesca();
        
        tipo.tipo = body.nombre;
    
        const errors = await validate(tipo);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al crear pais',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                const datosGuardados = await getRepository(TipoPesca).save(tipo);
                return res.status(201).json({
                        ok: true,
                        tipoPesca: datosGuardados
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
    }

    public async updateTipoPesca(req: Request, res: Response): Promise<Response> {

        const id = req.params.id;
        const body = req.body;

        const tipo = await getRepository(TipoPesca).findOne(id);

        if( !tipo ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un tipo de pesca con ese id',
                errors: {message: 'No existe un tipo de pesca con ese id.'}
            });
        }

        const tipoAdd = new TipoPesca();
        tipo.tipo = body.nombre;

        const errors = await validate(tipo);

        if (errors.length > 0) {
            return res.status(400).json({
                ok: false,
                mensaje: 'ERROR al actualizar el tipo de pesca',
                mesajeError: errors[0].constraints.isNotEmpty,
                errors
            });
        } else {
            try {

                getRepository(TipoPesca).merge(tipo, tipoAdd);
                const datos = await getRepository(TipoPesca).save(tipo);
                return res.status(200).json({
                    ok: true,
                    TipoPesca: datos
                });
                
            } catch (error) {

                return res.status(400).json({
                    ok: false,
                    err: error
                });
            }
        }
        
        
    }

    public async deleteTipoPesca(req: Request, res: Response): Promise<Response> {

        const ER_ROW_IS_REFERENCED_2 = 'ER_ROW_IS_REFERENCED_2';

        const id = req.params.id;
        const tipo = await getRepository(TipoPesca).findOne(id);
        if( !tipo ){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe un pais con id',
                errors: {message: 'No existe un pais con ese id.'}
            });
        }

        try {

            await getRepository(TipoPesca).delete(tipo.id);  
            
            return res.status(200).json({
                ok: false,
                mensaje: 'El tipo de pesca a sido eliminado',
                tipo     
            });

        } catch (error) {

            if(error.code === ER_ROW_IS_REFERENCED_2 ) {
                return res.status(400).json({
                        ok: false,
                        mensaje: 'ERROR el tipo de pesca esta vinculado con otra tabla',
                        error: 'No se puede eliminar o actualizar una fila principal: falla una restricción de clave externa',
                        err: error
                });
            }

            return res.status(400).json({
                ok: false,
                err: error
            });
        }
     
    }
}