import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Especie } from './especie';
import { IsNotEmpty } from 'class-validator';

@Entity('Arte_pesca')
export class ArtePesca {

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
        length: 45
    })
    @IsNotEmpty({ message: 'El nombre de la Arte de pesca es requerido' })
    nombre: string

    @OneToMany(type => Especie, especie => especie.id)
    especie: Especie[];

}