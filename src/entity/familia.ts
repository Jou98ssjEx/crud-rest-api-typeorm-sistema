import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Especie } from './especie';
import { IsNotEmpty } from 'class-validator';

@Entity('Familia')
export class Familia {

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
        length: 200
    })
    @IsNotEmpty({ message: 'El nombre de la familia es requerido' })
    nombre: string

    @OneToMany(type => Especie, especie => especie.id)
    especie: Especie[];

}