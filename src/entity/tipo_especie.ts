import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Especie } from './especie';
import { IsNotEmpty } from 'class-validator';

@Entity('Tipo_especie')
export class TipoEspecie {

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column({
        type: 'varchar',
        nullable: false,
        length: 45
    })
    @IsNotEmpty({ message: 'El nombre del Tipo de especie es requerido' })
    tipo: string

    @OneToMany(type => Especie, especie => especie.id)
    especie: Especie[];

}