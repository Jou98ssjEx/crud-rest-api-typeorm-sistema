import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { User } from './user';

export enum UserRole {
    ADMIN = "ADMIN_ROLE",
    USER = "USER_ROLE"
}

@Entity('Rol')
export class Rol {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "enum",
        enum: UserRole,
        default: UserRole.USER
    })
    role: UserRole

    @OneToMany(type => User, user => user.id)
    user: User[];

}