import { Entity, Column, PrimaryGeneratedColumn, ManyToOne} from 'typeorm';
import { Especie } from './especie';


@Entity('Location')
export class Location {

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    localidad: string

    @Column()
    latitud: string

    @Column()
    longitud: string

    //  @OneToMany(type => Especie, especie=> especie.id)
    //  especie: Especie[];

    //  @OneToMany(type => Especie, especie => especie.id)
    //  especie: Especie[];

    @ManyToOne((type) => Especie, (especie) => especie.location)
    especie: Especie;
}