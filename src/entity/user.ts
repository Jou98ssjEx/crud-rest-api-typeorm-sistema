import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  Unique,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import { IsEmail, IsNotEmpty, Length } from "class-validator";
import { Rol } from "./rol";

@Entity("Usuario")
export class User {
  @PrimaryGeneratedColumn("uuid")
  id: number;

  @Column({
    type: "varchar",
    nullable: false,
    length: 50,
  })
  @IsNotEmpty({ message: "El nombre es requerido" })
  nombre_usuario: string;

  @Column({
    type: "varchar",
    nullable: false,
    length: 50,
  })
  @IsNotEmpty({ message: "El apellido es requerido" })
  apellido_usuario: string;

  @Column({ unique: true })
  @IsEmail({}, { message: "Email incorrecto" })
  @IsNotEmpty({ message: "El correo es obligatorio" })
  email_usuario: string;

  @Column()
  @Length(6, 100, {
    message:
      "La contraseña debe tener al menos 6 pero no más de 30 caracteres.",
  })
  @IsNotEmpty({ message: "La contraseña es obligatoria" })
  pass_usuario: string;

  @Column({
    type: "varchar",
    nullable: false,
    length: 50,
  })
  @IsNotEmpty({ message: "El usuario es obligatorio" })
  user_name: string;

  @Column({ default: "" })
  foto: string;

  @Column({ default: "" })
  foto_resize: string;

  // @CreateDateColumn()
  // CREATED_AT: 'string';

  // @UpdateDateColumn()
  // UPDATED_AT: 'string';

  @ManyToOne((type) => Rol, (rol) => rol.id)
  rol: Rol;
}
