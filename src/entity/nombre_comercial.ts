import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Especie } from './especie';
import { IsNotEmpty } from 'class-validator';

@Entity('Nombre_comercial')
export class NombreComercial {

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    @IsNotEmpty({ message: 'El nombre es requerido' })
    nombre: string

    @Column()
    detalle: string

    @ManyToOne(type => Especie, especie => especie.nombreComercial)
    especie: Especie;

}