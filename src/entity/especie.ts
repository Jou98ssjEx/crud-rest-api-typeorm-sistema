import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  JoinColumn,
  OneToOne,
} from "typeorm";
import { Familia } from "./familia";
import { ArtePesca } from "./arte_pesca";
import { TipoPesca } from "./tipo_pesca";
import { TipoEspecie } from "./tipo_especie";
import { Foto } from "./foto";
import { Veda } from "./veda";
import { NombreComercial } from "./nombre_comercial";
import { NombreLocal } from "./nombre_local";
import { Medida } from './medidas';
import { Location } from './location';
import { type } from "os";
import { from } from "core-js/fn/array";

@Entity("Especie")
export class Especie {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ default: "" })
  img: string;

  @Column({ default: "" })
  img_resize: string;

  @Column()
  nombre_comun: string;

  @Column()
  dist_geo: string;

  @Column()
  talla: string;

  @Column()
  importancia: string;

  @ManyToOne((type) => Familia, (familia) => familia.id)
  familia: Familia;

  @ManyToOne((type) => ArtePesca, (artePesca) => artePesca.id)
  artePesca: ArtePesca;

  @ManyToOne((type) => TipoPesca, (tipoPesca) => tipoPesca.id)
  tipoPesca: TipoPesca;

  @ManyToOne((type) => TipoEspecie, (tipoEspecie) => tipoEspecie.id)
  tipoEspecie: TipoEspecie;

  @ManyToOne((type) => Medida, (medidas) => medidas.unidad)
  medida: Medida;

  @OneToMany((type) => Foto, (foto) => foto.especie)
  fotos: Foto[];

  @OneToMany((type) => Veda, (veda) => veda.especie)
  veda: Veda[];

  @OneToMany((type) => Location, (location) => location.especie)
  location: Location[];

  @OneToMany(
    (type) => NombreComercial,
    (nombreComercial) => nombreComercial.especie
  )
  nombreComercial: NombreComercial[];

  @OneToMany((type) => NombreLocal, (nombreLocal) => nombreLocal.especie)
  nombreLocal: NombreLocal[];

 
}