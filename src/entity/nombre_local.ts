import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Especie } from './especie';
import { IsNotEmpty } from 'class-validator';
import { Paises } from './pais';

@Entity('Nombre_local')
export class NombreLocal {

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    @IsNotEmpty({ message: 'El nombre del local es requerido' })
    nombre_local: string

    @ManyToOne(type => Especie, especie => especie.nombreLocal)
    especie: Especie;

    @ManyToOne(type => Paises, paises => paises.nombreLocal)
    paises: Paises;

}