import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Especie } from "./especie";
import { IsNotEmpty } from "class-validator";

@Entity("Veda")
export class Veda {
  @PrimaryGeneratedColumn("uuid")
  id: number;

  @Column()
  @IsNotEmpty({ message: "La Fecha de inicio es requerido" })
  inicio: Date;

  @Column()
  @IsNotEmpty({ message: "La Fecha de fin es requerido" })
  fin: Date;

  @Column({ default: false })
  isVeda: boolean;

  @ManyToOne((type) => Especie, (especie) => especie.veda)
  especie: Especie;
}
