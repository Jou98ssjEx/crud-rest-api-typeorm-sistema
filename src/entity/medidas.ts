import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinTable } from 'typeorm';
import { Especie } from './especie';


@Entity('Medida')
export class Medida {

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    unidad: string

    // @Column()
    // valor: string

    //  @OneToMany(type => Especie, especie=> especie.id)
    //  especie: Especie[];

     @OneToMany(type => Especie, especie => especie.id)
     especie: Especie[];

}