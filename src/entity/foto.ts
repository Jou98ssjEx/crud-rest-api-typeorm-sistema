import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { Especie } from "./especie";
import { IsNotEmpty } from "class-validator";

@Entity("Foto")
export class Foto {
  @PrimaryGeneratedColumn("uuid")
  id: number;

  @Column({
    type: "varchar",
    nullable: false,
    length: 255,
  })
  @IsNotEmpty({ message: "El archivo es requerido" })
  archivo: string;

  @Column({
    type: "varchar",
    nullable: false,
    length: 255,
  })
  @IsNotEmpty({ message: "El archivo es requerido" })
  resize: string;

  @ManyToOne((type) => Especie, (especie) => especie.fotos)
  especie: Especie;
}
