import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinTable } from 'typeorm';
import { NombreLocal } from './nombre_local';
import { IsNotEmpty } from 'class-validator';

@Entity('Paises')
export class Paises {

    @PrimaryGeneratedColumn('uuid')
    id: number;

    @Column()
    @IsNotEmpty({ message: 'El iso del pais es requerido' })
    iso: string

    @Column()
    @IsNotEmpty({ message: 'El nombre del pais es requerido' })
    nombre: string

    @OneToMany(type => NombreLocal, nombreLocal => nombreLocal.paises)
    nombreLocal: NombreLocal[];

}