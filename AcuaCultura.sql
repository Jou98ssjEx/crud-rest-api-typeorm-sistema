-- Insertar Rol
INSERT INTO `db_acuacultura`.`Rol` (`id`,`role`) VALUES (1,'USER_ROLE');
INSERT INTO `db_acuacultura`.`Rol` (`id`,`role`) VALUES (2,'ADMIN_ROLE');
-- Insertar familia
INSERT INTO `jou`.`Familia` (`id`, `nombre`) VALUES ('1', 'Coryphaenidae');
INSERT INTO `jou`.`Familia` (`id`, `nombre`) VALUES ('2', 'Scombridae');
INSERT INTO `jou`.`Familia` (`id`, `nombre`) VALUES ('3', 'Malacanthidae');
-- Insertar Arte de pesca
INSERT INTO `jou`.`Arte_pesca` (`id`, `nombre`) VALUES ('1', 'Arrastre');
INSERT INTO `jou`.`Arte_pesca` (`id`, `nombre`) VALUES ('2', 'Cerco');
INSERT INTO `jou`.`Arte_pesca` (`id`, `nombre`) VALUES ('3', 'Dragas');
INSERT INTO `jou`.`Arte_pesca` (`id`, `nombre`) VALUES ('4', 'Lineas y Anzuelo');
-- Insertar Tipo de pesca
INSERT INTO `jou`.`Tipo_pesca` (`id`, `tipo`) VALUES ('1', 'Pesca con mosca');
INSERT INTO `jou`.`Tipo_pesca` (`id`, `tipo`) VALUES ('2', 'Caprfishing');
INSERT INTO `jou`.`Tipo_pesca` (`id`, `tipo`) VALUES ('3', 'Captura y devolución');
INSERT INTO `jou`.`Tipo_pesca` (`id`, `tipo`) VALUES ('4', 'Pesca boloñesa');
INSERT INTO `jou`.`Tipo_pesca` (`id`, `tipo`) VALUES ('5', 'A la inglesa');
-- Insertar Tipo de Especie
INSERT INTO `jou`.`Tipo_especie` (`id`, `tipo`) VALUES ('1', 'Dorado');
INSERT INTO `jou`.`Tipo_especie` (`id`, `tipo`) VALUES ('2', 'Atún Ojo Grande');
INSERT INTO `jou`.`Tipo_especie` (`id`, `tipo`) VALUES ('3', 'Cabezudo');
-- Insertar Especie
INSERT INTO `jou`.`Especie` (`id`, `dist_geo`, `talla`, `importancia`, `familiaId`, `artePescaId`, `tipoPescaId`, `tipoEspecieId`) 
VALUES ('1', '32º43\'N-37º00\'S', '210 cm', 'Cosmopolita', '1', '1', '1', '1');
INSERT INTO `jou`.`Especie` (`id`, `dist_geo`, `talla`, `importancia`, `familiaId`, `artePescaId`, `tipoPescaId`, `tipoEspecieId`) 
VALUES ('2', '32º43\'N-37º00\'S', '250 cm', 'Thunnus thynnus', '2', '1', '1', '2');
INSERT INTO `jou`.`Especie` (`id`, `dist_geo`, `talla`, `importancia`, `familiaId`, `artePescaId`, `tipoPescaId`, `tipoEspecieId`) 
VALUES ('3', '32º43\'N-06º00\'S', '45 cm', 'Caulolatilus hubbsi', '3', '1', '1', '3');
-- Insertar Nombre Comercial
-- DORADO
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('1', 'Dorado', '1', '52');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('2', 'Delfín', '1', '52');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('3', 'Dorado', '1', '60');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('4', 'Dorado de al mar', '1', '46');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('5', 'Palometa', '1', '46');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('6', 'Dorado', '1', '66');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('7', 'Dorado', '1', '146');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('8', 'Delfín', '1', '146');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('9', 'Doradilla', '1', '146');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('10', 'Doradillo', '1', '146');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('11', 'Dorado', '1', '157');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('12', 'Delfín', '1', '170');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('13', 'Dorado', '1', '170');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('14', 'Pez dorado', '1', '170');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('15', 'Dorado', '1', '173');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('16', 'Perico', '1', '173');
-- ATÚN OJO GRANDE
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('17', 'Atún', '2', '52');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('18', 'Atún de ojo grande', '2', '46');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('19', 'Albacora', '2', '66');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('20', 'Atún ojo grande', '2', '66');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('21', 'Bigeye', '2', '66');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('22', 'Patudo', '2', '66');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('23', 'Tuna', '2', '66');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('24', 'Atún', '2', '146');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('25', 'Atún ojo grande', '2', '173');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('26', 'Bigeye', '2', '173');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('27', 'Patudo', '2', '173');
-- CABEZUDO
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('28', 'Cabezón', '3', '60');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('29', 'Pejeblanco', '3', '60');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('30', 'Cabezón', '3', '66');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('31', 'Cabezudo', '3', '66');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('32', 'Blanco', '3', '173');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('33', 'Blanquillo', '3', '173');
INSERT INTO `jou`.`Nombre_local` (`id`, `nombre_local`, `especieId`, `paisesId`) VALUES ('34', 'Cabezón', '3', '173');

-- Insertar Veda
INSERT INTO `jou`.`Veda` (`id`, `inicio`, `fin`, `especieId`) VALUES ('1', '2020/03/01', '2020/03/15', '1');


-- Insertar Paises
INSERT INTO `paises` VALUES(1, 'AF', 'Afganistán');
INSERT INTO `paises` VALUES(2, 'AX', 'Islas Gland');
INSERT INTO `paises` VALUES(3, 'AL', 'Albania');
INSERT INTO `paises` VALUES(4, 'DE', 'Alemania');
INSERT INTO `paises` VALUES(5, 'AD', 'Andorra');
INSERT INTO `paises` VALUES(6, 'AO', 'Angola');
INSERT INTO `paises` VALUES(7, 'AI', 'Anguilla');
INSERT INTO `paises` VALUES(8, 'AQ', 'Antártida');
INSERT INTO `paises` VALUES(9, 'AG', 'Antigua y Barbuda');
INSERT INTO `paises` VALUES(10, 'AN', 'Antillas Holandesas');
INSERT INTO `paises` VALUES(11, 'SA', 'Arabia Saudí');
INSERT INTO `paises` VALUES(12, 'DZ', 'Argelia');
INSERT INTO `paises` VALUES(13, 'AR', 'Argentina');
INSERT INTO `paises` VALUES(14, 'AM', 'Armenia');
INSERT INTO `paises` VALUES(15, 'AW', 'Aruba');
INSERT INTO `paises` VALUES(16, 'AU', 'Australia');
INSERT INTO `paises` VALUES(17, 'AT', 'Austria');
INSERT INTO `paises` VALUES(18, 'AZ', 'Azerbaiyán');
INSERT INTO `paises` VALUES(19, 'BS', 'Bahamas');
INSERT INTO `paises` VALUES(20, 'BH', 'Bahréin');
INSERT INTO `paises` VALUES(21, 'BD', 'Bangladesh');
INSERT INTO `paises` VALUES(22, 'BB', 'Barbados');
INSERT INTO `paises` VALUES(23, 'BY', 'Bielorrusia');
INSERT INTO `paises` VALUES(24, 'BE', 'Bélgica');
INSERT INTO `paises` VALUES(25, 'BZ', 'Belice');
INSERT INTO `paises` VALUES(26, 'BJ', 'Benin');
INSERT INTO `paises` VALUES(27, 'BM', 'Bermudas');
INSERT INTO `paises` VALUES(28, 'BT', 'Bhután');
INSERT INTO `paises` VALUES(29, 'BO', 'Bolivia');
INSERT INTO `paises` VALUES(30, 'BA', 'Bosnia y Herzegovina');
INSERT INTO `paises` VALUES(31, 'BW', 'Botsuana');
INSERT INTO `paises` VALUES(32, 'BV', 'Isla Bouvet');
INSERT INTO `paises` VALUES(33, 'BR', 'Brasil');
INSERT INTO `paises` VALUES(34, 'BN', 'Brunéi');
INSERT INTO `paises` VALUES(35, 'BG', 'Bulgaria');
INSERT INTO `paises` VALUES(36, 'BF', 'Burkina Faso');
INSERT INTO `paises` VALUES(37, 'BI', 'Burundi');
INSERT INTO `paises` VALUES(38, 'CV', 'Cabo Verde');
INSERT INTO `paises` VALUES(39, 'KY', 'Islas Caimán');
INSERT INTO `paises` VALUES(40, 'KH', 'Camboya');
INSERT INTO `paises` VALUES(41, 'CM', 'Camerún');
INSERT INTO `paises` VALUES(42, 'CA', 'Canadá');
INSERT INTO `paises` VALUES(43, 'CF', 'República Centroafricana');
INSERT INTO `paises` VALUES(44, 'TD', 'Chad');
INSERT INTO `paises` VALUES(45, 'CZ', 'República Checa');
INSERT INTO `paises` VALUES(46, 'CL', 'Chile');
INSERT INTO `paises` VALUES(47, 'CN', 'China');
INSERT INTO `paises` VALUES(48, 'CY', 'Chipre');
INSERT INTO `paises` VALUES(49, 'CX', 'Isla de Navidad');
INSERT INTO `paises` VALUES(50, 'VA', 'Ciudad del Vaticano');
INSERT INTO `paises` VALUES(51, 'CC', 'Islas Cocos');
INSERT INTO `paises` VALUES(52, 'CO', 'Colombia');
INSERT INTO `paises` VALUES(53, 'KM', 'Comoras');
INSERT INTO `paises` VALUES(54, 'CD', 'República Democrática del Congo');
INSERT INTO `paises` VALUES(55, 'CG', 'Congo');
INSERT INTO `paises` VALUES(56, 'CK', 'Islas Cook');
INSERT INTO `paises` VALUES(57, 'KP', 'Corea del Norte');
INSERT INTO `paises` VALUES(58, 'KR', 'Corea del Sur');
INSERT INTO `paises` VALUES(59, 'CI', 'Costa de Marfil');
INSERT INTO `paises` VALUES(60, 'CR', 'Costa Rica');
INSERT INTO `paises` VALUES(61, 'HR', 'Croacia');
INSERT INTO `paises` VALUES(62, 'CU', 'Cuba');
INSERT INTO `paises` VALUES(63, 'DK', 'Dinamarca');
INSERT INTO `paises` VALUES(64, 'DM', 'Dominica');
INSERT INTO `paises` VALUES(65, 'DO', 'República Dominicana');
INSERT INTO `paises` VALUES(66, 'EC', 'Ecuador');
INSERT INTO `paises` VALUES(67, 'EG', 'Egipto');
INSERT INTO `paises` VALUES(68, 'SV', 'El Salvador');
INSERT INTO `paises` VALUES(69, 'AE', 'Emiratos Árabes Unidos');
INSERT INTO `paises` VALUES(70, 'ER', 'Eritrea');
INSERT INTO `paises` VALUES(71, 'SK', 'Eslovaquia');
INSERT INTO `paises` VALUES(72, 'SI', 'Eslovenia');
INSERT INTO `paises` VALUES(73, 'ES', 'España');
INSERT INTO `paises` VALUES(74, 'UM', 'Islas ultramarinas de Estados Unidos');
INSERT INTO `paises` VALUES(75, 'US', 'Estados Unidos');
INSERT INTO `paises` VALUES(76, 'EE', 'Estonia');
INSERT INTO `paises` VALUES(77, 'ET', 'Etiopía');
INSERT INTO `paises` VALUES(78, 'FO', 'Islas Feroe');
INSERT INTO `paises` VALUES(79, 'PH', 'Filipinas');
INSERT INTO `paises` VALUES(80, 'FI', 'Finlandia');
INSERT INTO `paises` VALUES(81, 'FJ', 'Fiyi');
INSERT INTO `paises` VALUES(82, 'FR', 'Francia');
INSERT INTO `paises` VALUES(83, 'GA', 'Gabón');
INSERT INTO `paises` VALUES(84, 'GM', 'Gambia');
INSERT INTO `paises` VALUES(85, 'GE', 'Georgia');
INSERT INTO `paises` VALUES(86, 'GS', 'Islas Georgias del Sur y Sandwich del Sur');
INSERT INTO `paises` VALUES(87, 'GH', 'Ghana');
INSERT INTO `paises` VALUES(88, 'GI', 'Gibraltar');
INSERT INTO `paises` VALUES(89, 'GD', 'Granada');
INSERT INTO `paises` VALUES(90, 'GR', 'Grecia');
INSERT INTO `paises` VALUES(91, 'GL', 'Groenlandia');
INSERT INTO `paises` VALUES(92, 'GP', 'Guadalupe');
INSERT INTO `paises` VALUES(93, 'GU', 'Guam');
INSERT INTO `paises` VALUES(94, 'GT', 'Guatemala');
INSERT INTO `paises` VALUES(95, 'GF', 'Guayana Francesa');
INSERT INTO `paises` VALUES(96, 'GN', 'Guinea');
INSERT INTO `paises` VALUES(97, 'GQ', 'Guinea Ecuatorial');
INSERT INTO `paises` VALUES(98, 'GW', 'Guinea-Bissau');
INSERT INTO `paises` VALUES(99, 'GY', 'Guyana');
INSERT INTO `paises` VALUES(100, 'HT', 'Haití');
INSERT INTO `paises` VALUES(101, 'HM', 'Islas Heard y McDonald');
INSERT INTO `paises` VALUES(102, 'HN', 'Honduras');
INSERT INTO `paises` VALUES(103, 'HK', 'Hong Kong');
INSERT INTO `paises` VALUES(104, 'HU', 'Hungría');
INSERT INTO `paises` VALUES(105, 'IN', 'India');
INSERT INTO `paises` VALUES(106, 'ID', 'Indonesia');
INSERT INTO `paises` VALUES(107, 'IR', 'Irán');
INSERT INTO `paises` VALUES(108, 'IQ', 'Iraq');
INSERT INTO `paises` VALUES(109, 'IE', 'Irlanda');
INSERT INTO `paises` VALUES(110, 'IS', 'Islandia');
INSERT INTO `paises` VALUES(111, 'IL', 'Israel');
INSERT INTO `paises` VALUES(112, 'IT', 'Italia');
INSERT INTO `paises` VALUES(113, 'JM', 'Jamaica');
INSERT INTO `paises` VALUES(114, 'JP', 'Japón');
INSERT INTO `paises` VALUES(115, 'JO', 'Jordania');
INSERT INTO `paises` VALUES(116, 'KZ', 'Kazajstán');
INSERT INTO `paises` VALUES(117, 'KE', 'Kenia');
INSERT INTO `paises` VALUES(118, 'KG', 'Kirguistán');
INSERT INTO `paises` VALUES(119, 'KI', 'Kiribati');
INSERT INTO `paises` VALUES(120, 'KW', 'Kuwait');
INSERT INTO `paises` VALUES(121, 'LA', 'Laos');
INSERT INTO `paises` VALUES(122, 'LS', 'Lesotho');
INSERT INTO `paises` VALUES(123, 'LV', 'Letonia');
INSERT INTO `paises` VALUES(124, 'LB', 'Líbano');
INSERT INTO `paises` VALUES(125, 'LR', 'Liberia');
INSERT INTO `paises` VALUES(126, 'LY', 'Libia');
INSERT INTO `paises` VALUES(127, 'LI', 'Liechtenstein');
INSERT INTO `paises` VALUES(128, 'LT', 'Lituania');
INSERT INTO `paises` VALUES(129, 'LU', 'Luxemburgo');
INSERT INTO `paises` VALUES(130, 'MO', 'Macao');
INSERT INTO `paises` VALUES(131, 'MK', 'ARY Macedonia');
INSERT INTO `paises` VALUES(132, 'MG', 'Madagascar');
INSERT INTO `paises` VALUES(133, 'MY', 'Malasia');
INSERT INTO `paises` VALUES(134, 'MW', 'Malawi');
INSERT INTO `paises` VALUES(135, 'MV', 'Maldivas');
INSERT INTO `paises` VALUES(136, 'ML', 'Malí');
INSERT INTO `paises` VALUES(137, 'MT', 'Malta');
INSERT INTO `paises` VALUES(138, 'FK', 'Islas Malvinas');
INSERT INTO `paises` VALUES(139, 'MP', 'Islas Marianas del Norte');
INSERT INTO `paises` VALUES(140, 'MA', 'Marruecos');
INSERT INTO `paises` VALUES(141, 'MH', 'Islas Marshall');
INSERT INTO `paises` VALUES(142, 'MQ', 'Martinica');
INSERT INTO `paises` VALUES(143, 'MU', 'Mauricio');
INSERT INTO `paises` VALUES(144, 'MR', 'Mauritania');
INSERT INTO `paises` VALUES(145, 'YT', 'Mayotte');
INSERT INTO `paises` VALUES(146, 'MX', 'México');
INSERT INTO `paises` VALUES(147, 'FM', 'Micronesia');
INSERT INTO `paises` VALUES(148, 'MD', 'Moldavia');
INSERT INTO `paises` VALUES(149, 'MC', 'Mónaco');
INSERT INTO `paises` VALUES(150, 'MN', 'Mongolia');
INSERT INTO `paises` VALUES(151, 'MS', 'Montserrat');
INSERT INTO `paises` VALUES(152, 'MZ', 'Mozambique');
INSERT INTO `paises` VALUES(153, 'MM', 'Myanmar');
INSERT INTO `paises` VALUES(154, 'NA', 'Namibia');
INSERT INTO `paises` VALUES(155, 'NR', 'Nauru');
INSERT INTO `paises` VALUES(156, 'NP', 'Nepal');
INSERT INTO `paises` VALUES(157, 'NI', 'Nicaragua');
INSERT INTO `paises` VALUES(158, 'NE', 'Níger');
INSERT INTO `paises` VALUES(159, 'NG', 'Nigeria');
INSERT INTO `paises` VALUES(160, 'NU', 'Niue');
INSERT INTO `paises` VALUES(161, 'NF', 'Isla Norfolk');
INSERT INTO `paises` VALUES(162, 'NO', 'Noruega');
INSERT INTO `paises` VALUES(163, 'NC', 'Nueva Caledonia');
INSERT INTO `paises` VALUES(164, 'NZ', 'Nueva Zelanda');
INSERT INTO `paises` VALUES(165, 'OM', 'Omán');
INSERT INTO `paises` VALUES(166, 'NL', 'Países Bajos');
INSERT INTO `paises` VALUES(167, 'PK', 'Pakistán');
INSERT INTO `paises` VALUES(168, 'PW', 'Palau');
INSERT INTO `paises` VALUES(169, 'PS', 'Palestina');
INSERT INTO `paises` VALUES(170, 'PA', 'Panamá');
INSERT INTO `paises` VALUES(171, 'PG', 'Papúa Nueva Guinea');
INSERT INTO `paises` VALUES(172, 'PY', 'Paraguay');
INSERT INTO `paises` VALUES(173, 'PE', 'Perú');
INSERT INTO `paises` VALUES(174, 'PN', 'Islas Pitcairn');
INSERT INTO `paises` VALUES(175, 'PF', 'Polinesia Francesa');
INSERT INTO `paises` VALUES(176, 'PL', 'Polonia');
INSERT INTO `paises` VALUES(177, 'PT', 'Portugal');
INSERT INTO `paises` VALUES(178, 'PR', 'Puerto Rico');
INSERT INTO `paises` VALUES(179, 'QA', 'Qatar');
INSERT INTO `paises` VALUES(180, 'GB', 'Reino Unido');
INSERT INTO `paises` VALUES(181, 'RE', 'Reunión');
INSERT INTO `paises` VALUES(182, 'RW', 'Ruanda');
INSERT INTO `paises` VALUES(183, 'RO', 'Rumania');
INSERT INTO `paises` VALUES(184, 'RU', 'Rusia');
INSERT INTO `paises` VALUES(185, 'EH', 'Sahara Occidental');
INSERT INTO `paises` VALUES(186, 'SB', 'Islas Salomón');
INSERT INTO `paises` VALUES(187, 'WS', 'Samoa');
INSERT INTO `paises` VALUES(188, 'AS', 'Samoa Americana');
INSERT INTO `paises` VALUES(189, 'KN', 'San Cristóbal y Nevis');
INSERT INTO `paises` VALUES(190, 'SM', 'San Marino');
INSERT INTO `paises` VALUES(191, 'PM', 'San Pedro y Miquelón');
INSERT INTO `paises` VALUES(192, 'VC', 'San Vicente y las Granadinas');
INSERT INTO `paises` VALUES(193, 'SH', 'Santa Helena');
INSERT INTO `paises` VALUES(194, 'LC', 'Santa Lucía');
INSERT INTO `paises` VALUES(195, 'ST', 'Santo Tomé y Príncipe');
INSERT INTO `paises` VALUES(196, 'SN', 'Senegal');
INSERT INTO `paises` VALUES(197, 'CS', 'Serbia y Montenegro');
INSERT INTO `paises` VALUES(198, 'SC', 'Seychelles');
INSERT INTO `paises` VALUES(199, 'SL', 'Sierra Leona');
INSERT INTO `paises` VALUES(200, 'SG', 'Singapur');
INSERT INTO `paises` VALUES(201, 'SY', 'Siria');
INSERT INTO `paises` VALUES(202, 'SO', 'Somalia');
INSERT INTO `paises` VALUES(203, 'LK', 'Sri Lanka');
INSERT INTO `paises` VALUES(204, 'SZ', 'Suazilandia');
INSERT INTO `paises` VALUES(205, 'ZA', 'Sudáfrica');
INSERT INTO `paises` VALUES(206, 'SD', 'Sudán');
INSERT INTO `paises` VALUES(207, 'SE', 'Suecia');
INSERT INTO `paises` VALUES(208, 'CH', 'Suiza');
INSERT INTO `paises` VALUES(209, 'SR', 'Surinam');
INSERT INTO `paises` VALUES(210, 'SJ', 'Svalbard y Jan Mayen');
INSERT INTO `paises` VALUES(211, 'TH', 'Tailandia');
INSERT INTO `paises` VALUES(212, 'TW', 'Taiwán');
INSERT INTO `paises` VALUES(213, 'TZ', 'Tanzania');
INSERT INTO `paises` VALUES(214, 'TJ', 'Tayikistán');
INSERT INTO `paises` VALUES(215, 'IO', 'Territorio Británico del Océano Índico');
INSERT INTO `paises` VALUES(216, 'TF', 'Territorios Australes Franceses');
INSERT INTO `paises` VALUES(217, 'TL', 'Timor Oriental');
INSERT INTO `paises` VALUES(218, 'TG', 'Togo');
INSERT INTO `paises` VALUES(219, 'TK', 'Tokelau');
INSERT INTO `paises` VALUES(220, 'TO', 'Tonga');
INSERT INTO `paises` VALUES(221, 'TT', 'Trinidad y Tobago');
INSERT INTO `paises` VALUES(222, 'TN', 'Túnez');
INSERT INTO `paises` VALUES(223, 'TC', 'Islas Turcas y Caicos');
INSERT INTO `paises` VALUES(224, 'TM', 'Turkmenistán');
INSERT INTO `paises` VALUES(225, 'TR', 'Turquía');
INSERT INTO `paises` VALUES(226, 'TV', 'Tuvalu');
INSERT INTO `paises` VALUES(227, 'UA', 'Ucrania');
INSERT INTO `paises` VALUES(228, 'UG', 'Uganda');
INSERT INTO `paises` VALUES(229, 'UY', 'Uruguay');
INSERT INTO `paises` VALUES(230, 'UZ', 'Uzbekistán');
INSERT INTO `paises` VALUES(231, 'VU', 'Vanuatu');
INSERT INTO `paises` VALUES(232, 'VE', 'Venezuela');
INSERT INTO `paises` VALUES(233, 'VN', 'Vietnam');
INSERT INTO `paises` VALUES(234, 'VG', 'Islas Vírgenes Británicas');
INSERT INTO `paises` VALUES(235, 'VI', 'Islas Vírgenes de los Estados Unidos');
INSERT INTO `paises` VALUES(236, 'WF', 'Wallis y Futuna');
INSERT INTO `paises` VALUES(237, 'YE', 'Yemen');
INSERT INTO `paises` VALUES(238, 'DJ', 'Yibuti');
INSERT INTO `paises` VALUES(239, 'ZM', 'Zambia');
INSERT INTO `paises` VALUES(240, 'ZW', 'Zimbabue');






-- Ecuador
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPHBhdGggc3R5bGU9ImZpbGw6I0ZGREE0NDsiIGQ9Ik0wLDI1NkMwLDExNC42MTYsMTE0LjYxNiwwLDI1NiwwczI1NiwxMTQuNjE2LDI1NiwyNTZsLTI1NiwyMi4yNjFMMCwyNTZ6Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiNEODAwMjc7IiBkPSJNMzQuMjU2LDM4NEM3OC41MjEsNDYwLjUxNiwxNjEuMjQ1LDUxMiwyNTYsNTEyczE3Ny40NzktNTEuNDg0LDIyMS43NDQtMTI4TDI1NiwzNjcuMzA0TDM0LjI1NiwzODR6ICAiLz4KPHBhdGggc3R5bGU9ImZpbGw6IzAwNTJCNDsiIGQ9Ik00NzcuNzQ0LDM4NEM0OTkuNTI2LDM0Ni4zNDYsNTEyLDMwMi42MzEsNTEyLDI1NkgwYzAsNDYuNjMxLDEyLjQ3NCw5MC4zNDYsMzQuMjU2LDEyOEg0NzcuNzQ0eiIvPgo8Y2lyY2xlIHN0eWxlPSJmaWxsOiNGRkRBNDQ7IiBjeD0iMjU2IiBjeT0iMjU2IiByPSI4OS4wNDMiLz4KPHBhdGggc3R5bGU9ImZpbGw6IzMzOEFGMzsiIGQ9Ik0yNTYsMzExLjY1MmMtMzAuNjg3LDAtNTUuNjUyLTI0Ljk2Ni01NS42NTItNTUuNjUydi0zMy4zOTFjMC0zMC42ODcsMjQuOTY2LTU1LjY1Miw1NS42NTItNTUuNjUyICBzNTUuNjUyLDI0Ljk2Niw1NS42NTIsNTUuNjUyVjI1NkMzMTEuNjUyLDI4Ni42ODcsMjg2LjY4NywzMTEuNjUyLDI1NiwzMTEuNjUyeiIvPgo8cGF0aCBkPSJNMzQ1LjA0MywxMjIuNDM1aC02Ni43ODNjMC0xMi4yOTQtOS45NjctMjIuMjYxLTIyLjI2MS0yMi4yNjFzLTIyLjI2MSw5Ljk2Ny0yMi4yNjEsMjIuMjYxaC02Ni43ODMgIGMwLDEyLjI5NSwxMC43MDksMjIuMjYxLDIzLjAwMiwyMi4yNjFoLTAuNzQxYzAsMTIuMjk1LDkuOTY2LDIyLjI2MSwyMi4yNjEsMjIuMjYxYzAsMTIuMjk1LDkuOTY2LDIyLjI2MSwyMi4yNjEsMjIuMjYxaDQ0LjUyMiAgYzEyLjI5NSwwLDIyLjI2MS05Ljk2NiwyMi4yNjEtMjIuMjYxYzEyLjI5NSwwLDIyLjI2MS05Ljk2NiwyMi4yNjEtMjIuMjYxaC0wLjc0MUMzMzQuMzM1LDE0NC42OTYsMzQ1LjA0MywxMzQuNzI5LDM0NS4wNDMsMTIyLjQzNSAgeiIvPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K
-- Mexico
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPGNpcmNsZSBzdHlsZT0iZmlsbDojRjBGMEYwOyIgY3g9IjI1NiIgY3k9IjI1NiIgcj0iMjU2Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiNEODAwMjc7IiBkPSJNNTEyLDI1NmMwLTEwMS40OTQtNTkuMDY1LTE4OS4xOS0xNDQuNjk2LTIzMC41OTh2NDYxLjE5NUM0NTIuOTM1LDQ0NS4xOSw1MTIsMzU3LjQ5NCw1MTIsMjU2eiIvPgo8Zz4KCTxwYXRoIHN0eWxlPSJmaWxsOiM2REE1NDQ7IiBkPSJNMCwyNTZjMCwxMDEuNDk0LDU5LjA2NSwxODkuMTksMTQ0LjY5NiwyMzAuNTk4VjI1LjQwMkM1OS4wNjUsNjYuODEsMCwxNTQuNTA2LDAsMjU2eiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6IzZEQTU0NDsiIGQ9Ik0xODkuMjE3LDI1NmMwLDM2Ljg4MywyOS45LDY2Ljc4Myw2Ni43ODMsNjYuNzgzczY2Ljc4My0yOS45LDY2Ljc4My02Ni43ODN2LTIyLjI2MUgxODkuMjE3VjI1NnoiLz4KPC9nPgo8cGF0aCBzdHlsZT0iZmlsbDojRkY5ODExOyIgZD0iTTM0NS4wNDMsMjExLjQ3OGgtNjYuNzgzYzAtMTIuMjk0LTkuOTY3LTIyLjI2MS0yMi4yNjEtMjIuMjYxcy0yMi4yNjEsOS45NjctMjIuMjYxLDIyLjI2MWgtNjYuNzgzICBjMCwxMi4yOTUsMTAuNzA5LDIyLjI2MSwyMy4wMDIsMjIuMjYxaC0wLjc0MWMwLDEyLjI5NSw5Ljk2NiwyMi4yNjEsMjIuMjYxLDIyLjI2MWMwLDEyLjI5NSw5Ljk2NiwyMi4yNjEsMjIuMjYxLDIyLjI2MWg0NC41MjIgIGMxMi4yOTUsMCwyMi4yNjEtOS45NjYsMjIuMjYxLTIyLjI2MWMxMi4yOTUsMCwyMi4yNjEtOS45NjYsMjIuMjYxLTIyLjI2MWgtMC43NDJDMzM0LjMzNSwyMzMuNzM5LDM0NS4wNDMsMjIzLjc3MywzNDUuMDQzLDIxMS40NzggIHoiLz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==
-- Colombia
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ3My42ODEgNDczLjY4MSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDczLjY4MSA0NzMuNjgxOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPHBhdGggc3R5bGU9ImZpbGw6I0ZCRDAxNTsiIGQ9Ik0zMDUuNDU2LDIyNi43MDZoMTY3Ljk2N0M0NjguMTA1LDEwMC42MDQsMzY0LjIzOCwwLjAwMiwyMzYuODM1LDAuMDAyICBDMzAwLjM3LDAuMDAyLDMwMy42NDIsMTExLjc1OSwzMDUuNDU2LDIyNi43MDZ6Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiMyMTQyOEM7IiBkPSJNMzA1LjQ1NiwzNTEuMDM5aDEzOC44ODhjMTguNjc5LTMzLjg2OSwyOS4zMzctNzIuNzgzLDI5LjMzNy0xMTQuMTk1ICBjMC0zLjM5Ni0wLjExMi02Ljc3Mi0wLjI1NC0xMC4xMzhoLTE1Ny4xQzMxNy4wMDQsMjY5LjU5NSwzMTMuMzY5LDMxMi44OTksMzA1LjQ1NiwzNTEuMDM5eiIvPgo8Zz4KCTxwYXRoIHN0eWxlPSJmaWxsOiNDRDIwMkE7IiBkPSJNMjI5LjI1OCw0NzMuNTQ4Yy0wLjYwNi0wLjAxNS0xLjIxNS0wLjA0NS0xLjgyMS0wLjA2NyAgIEMyMjguMDM5LDQ3My41MDMsMjI4LjY0OSw0NzMuNTMzLDIyOS4yNTgsNDczLjU0OHoiLz4KCTxwYXRoIHN0eWxlPSJmaWxsOiNDRDIwMkE7IiBkPSJNMjM2LjgzNSw0NzMuNjc5Yzg5LjM4NywwLDE2Ny4xODUtNDkuNTM0LDIwNy41MDUtMTIyLjYzOUgzMDAuNDY3ICAgQzI4NS45Miw0MjEuMTE5LDI4MC4zOTcsNDczLjY3OSwyMzYuODM1LDQ3My42Nzl6Ii8+CjwvZz4KPHBhdGggc3R5bGU9ImZpbGw6I0ZGRkZGRjsiIGQ9Ik0yMzYuODM1LDQ3My42NzljLTIuNTM5LDAtNS4wNjMtMC4wNTItNy41NzYtMC4xMzFDMjMxLjc4Niw0NzMuNjI2LDIzNC4zMTQsNDczLjY3OSwyMzYuODM1LDQ3My42NzkgIHoiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0ZDRDczRTsiIGQ9Ik0wLjI1NCwyMjYuNzA2aDMzNC41OTVDMzMzLjAzOSwxMTEuNzU5LDMwMC4zNywwLjAwMiwyMzYuODM1LDAuMDAyICBDMTA5LjQzOSwwLjAwMiw1LjU3MiwxMDAuNjA0LDAuMjU0LDIyNi43MDZ6Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiMxRDRGOTU7IiBkPSJNMzM0Ljg0OSwyMjYuNzA2SDAuMjU0QzAuMTEyLDIzMC4wNzIsMCwyMzMuNDQ5LDAsMjM2Ljg0NGMwLDQxLjQxMiwxMC42NTgsODAuMzI2LDI5LjMzNywxMTQuMTk1ICBoMjk0LjY0NUMzMzEuODk1LDMxMi44OTksMzM1LjUzLDI2OS41OTUsMzM0Ljg0OSwyMjYuNzA2eiIvPgo8cGF0aCBzdHlsZT0iZmlsbDojRDExRjNFOyIgZD0iTTIyNy40MzMsNDczLjQ4YzAuNjA2LDAuMDIyLDEuMjE1LDAuMDUyLDEuODIxLDAuMDY3YzIuNTE3LDAuMDc5LDUuMDQxLDAuMTMxLDcuNTc2LDAuMTMxICBjNDMuNTYyLDAsNzIuNTk2LTUyLjU2LDg3LjE0My0xMjIuNjM5SDI5LjMzM0M2OC4yMzYsNDIxLjU2NCwxNDIuMDIyLDQ3MC4xNDUsMjI3LjQzMyw0NzMuNDh6Ii8+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=
-- Nicaragua
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPGNpcmNsZSBzdHlsZT0iZmlsbDojRjBGMEYwOyIgY3g9IjI1NiIgY3k9IjI1NiIgcj0iMjU2Ii8+CjxnPgoJPHBhdGggc3R5bGU9ImZpbGw6IzMzOEFGMzsiIGQ9Ik0yNTYsMEMxNTQuNTA2LDAsNjYuODEsNTkuMDY1LDI1LjQwMiwxNDQuNjk2aDQ2MS4xOTVDNDQ1LjE5LDU5LjA2NSwzNTcuNDkzLDAsMjU2LDB6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojMzM4QUYzOyIgZD0iTTI1Niw1MTJjMTAxLjQ5MywwLDE4OS4xOS01OS4wNjUsMjMwLjU5OC0xNDQuNjk2SDI1LjQwMkM2Ni44MSw0NTIuOTM1LDE1NC41MDYsNTEyLDI1Niw1MTJ6Ii8+CjwvZz4KPHBhdGggc3R5bGU9ImZpbGw6I0ZGREE0NDsiIGQ9Ik0yNTYsMTc4LjA4N2MtNDMuMDMsMC03Ny45MTMsMzQuODgzLTc3LjkxMyw3Ny45MTNTMjEyLjk3LDMzMy45MTMsMjU2LDMzMy45MTMgIFMzMzMuOTEzLDI5OS4wMywzMzMuOTEzLDI1NlMyOTkuMDMsMTc4LjA4NywyNTYsMTc4LjA4N3ogTTI1NiwzMDAuNTIyYy0yNC41ODgsMC00NC41MjItMTkuOTMyLTQ0LjUyMi00NC41MjIgIHMxOS45MzMtNDQuNTIyLDQ0LjUyMi00NC41MjJzNDQuNTIyLDE5LjkzMiw0NC41MjIsNDQuNTIyUzI4MC41ODgsMzAwLjUyMiwyNTYsMzAwLjUyMnoiLz4KPHBvbHlnb24gc3R5bGU9ImZpbGw6IzAwNTJCNDsiIHBvaW50cz0iMjk0LjU1OCwyNjcuMTMgMjU2LDI1NiAyMTcuNDQyLDI2Ny4xMyAyMDQuNTksMjg5LjM5MSAzMDcuNDA5LDI4OS4zOTEgIi8+Cjxwb2x5Z29uIHN0eWxlPSJmaWxsOiMzMzhBRjM7IiBwb2ludHM9IjI1NiwyMDAuMzQ4IDIzMC4yOTUsMjQ0Ljg3IDI1NiwyNTYgMjgxLjcwNSwyNDQuODcgIi8+Cjxwb2x5Z29uIHN0eWxlPSJmaWxsOiM2REE1NDQ7IiBwb2ludHM9IjIxNy40NDIsMjY3LjEzIDI5NC41NTgsMjY3LjEzIDI4MS43MDUsMjQ0Ljg3IDIzMC4yOTUsMjQ0Ljg3ICIvPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K
-- Panama
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPGNpcmNsZSBzdHlsZT0iZmlsbDojRjBGMEYwOyIgY3g9IjI1NiIgY3k9IjI1NiIgcj0iMjU2Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiMwMDUyQjQ7IiBkPSJNMCwyNTZjMCwxNDEuMzg0LDExNC42MTYsMjU2LDI1NiwyNTZjMC05Ny4zNTUsMC0yNTYsMC0yNTZTODkuMDQzLDI1NiwwLDI1NnoiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0Q4MDAyNzsiIGQ9Ik0yNTYsMGMxNDEuMzg0LDAsMjU2LDExNC42MTYsMjU2LDI1NmMtOTcuMzU1LDAtMjU2LDAtMjU2LDBTMjU2LDg5LjA0MywyNTYsMHoiLz4KPHBvbHlnb24gc3R5bGU9ImZpbGw6IzAwNTJCNDsiIHBvaW50cz0iMTUyLjM4OSw4OS4wNDMgMTY4Ljk2NiwxNDAuMDYxIDIyMi42MDksMTQwLjA2MSAxNzkuMjExLDE3MS41OTEgMTk1Ljc4NywyMjIuNjA5ICAgMTUyLjM4OSwxOTEuMDc4IDEwOC45OTEsMjIyLjYwOSAxMjUuNTY3LDE3MS41OTEgODIuMTY5LDE0MC4wNjEgMTM1LjgxMiwxNDAuMDYxICIvPgo8cG9seWdvbiBzdHlsZT0iZmlsbDojRDgwMDI3OyIgcG9pbnRzPSIzNTkuNjExLDI4OS4zOTEgMzc2LjE4OCwzNDAuNDA5IDQyOS44MzEsMzQwLjQwOSAzODYuNDMyLDM3MS45MzkgNDAzLjAwOSw0MjIuOTU3ICAgMzU5LjYxMSwzOTEuNDI2IDMxNi4yMTMsNDIyLjk1NyAzMzIuNzg5LDM3MS45MzkgMjg5LjM5MSwzNDAuNDA5IDM0My4wMzQsMzQwLjQwOSAiLz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==
-- Perú
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ3My42OCA0NzMuNjgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQ3My42OCA0NzMuNjg7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8cGF0aCBzdHlsZT0iZmlsbDojRDMyMDI3OyIgZD0iTTMxNS44LDEzLjUzNWwtMjcuNjQsNzYuNjI0YzI1LjUxMSw4NC4yLDI1LjUxMSwyMDkuMTYyLDAsMjkzLjM1OGwyNy42MzksNzYuNjI0ICBjOTEuOTc1LTMyLjUyNCwxNTcuODgxLTEyMC4yMDEsMTU3Ljg4MS0yMjMuMzFDNDczLjY4MSwxMzMuNzMzLDQwNy43NzQsNDYuMDU1LDMxNS44LDEzLjUzNXoiLz4KPGc+Cgk8cGF0aCBzdHlsZT0iZmlsbDojRTdFN0U3OyIgZD0iTTMxNS44LDkwLjE1OVYxMy41MzVDMjkxLjEsNC44LDI2NC41MzQsMC4wMDIsMjM2LjgzOCwwLjAwMiAgIEMyNzMuMzU1LDAuMDAyLDIyMi43MTgsMTIzLjc3NCwzMTUuOCw5MC4xNTl6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojRTdFN0U3OyIgZD0iTTIzNi44MzgsNDczLjY3OGMyNy42OTUsMCw1NC4yNjEtNC43OTgsNzguOTYxLTEzLjUzNFYzODMuNTIgICBDMjIzLjY1MywzNzQuMzI2LDIzNi44MzgsNDczLjY3OCwyMzYuODM4LDQ3My42Nzh6Ii8+CjwvZz4KPGc+Cgk8cGF0aCBzdHlsZT0iZmlsbDojRTczQjM2OyIgZD0iTTAsMjM2LjgzN0MwLDM0MC4yODksNjYuMzU1LDQyOC4xOTgsMTU4LjgwNSw0NjAuNDZWMTMuMjIxQzY2LjM1NSw0NS40ODMsMCwxMzMuMzg5LDAsMjM2LjgzN3oiLz4KCTxwYXRoIHN0eWxlPSJmaWxsOiNFNzNCMzY7IiBkPSJNMzE1LjgsOTAuMTU5djI5My4zNThDMzQxLjMxMSwyOTkuMzIxLDM0MS4zMTEsMTc0LjM1OCwzMTUuOCw5MC4xNTl6Ii8+CjwvZz4KPHBhdGggc3R5bGU9ImZpbGw6I0YzRjFGMjsiIGQ9Ik0zMTUuOCwzODMuNTIxVjkwLjE1OWMtMTYuMTI5LTUzLjIyMi00Mi40NDQtOTAuMTU3LTc4Ljk2MS05MC4xNTcgIGMtMjcuMzUxLDAtNTMuNTkyLDQuNjkzLTc4LjAzNCwxMy4yMTl2NDQ3LjIzOGMyNC40NDIsOC41MjYsNTAuNjgyLDEzLjIxOSw3OC4wMzQsMTMuMjE5ICBDMjczLjM1NSw0NzMuNjc4LDI5OS42NzEsNDM2Ljc0MywzMTUuOCwzODMuNTIxeiIvPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K
-- Costa Rica
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPGNpcmNsZSBzdHlsZT0iZmlsbDojRjBGMEYwOyIgY3g9IjI1NiIgY3k9IjI1NiIgcj0iMjU2Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiNEODAwMjc7IiBkPSJNNDk2LjA3NywxNjYuOTU3SDE1LjkyM0M1LjYzMywxOTQuNjksMCwyMjQuNjg2LDAsMjU2czUuNjMzLDYxLjMxLDE1LjkyMyw4OS4wNDNoNDgwLjE1NSAgQzUwNi4zNjgsMzE3LjMxLDUxMiwyODcuMzE0LDUxMiwyNTZTNTA2LjM2OCwxOTQuNjksNDk2LjA3NywxNjYuOTU3eiIvPgo8Zz4KCTxwYXRoIHN0eWxlPSJmaWxsOiMwMDUyQjQ7IiBkPSJNMjU2LDBDMTc4LjQwOSwwLDEwOC44ODYsMzQuNTI0LDYxLjkzOSw4OS4wNDNINDUwLjA2QzQwMy4xMTQsMzQuNTI0LDMzMy41OTEsMCwyNTYsMHoiLz4KCTxwYXRoIHN0eWxlPSJmaWxsOiMwMDUyQjQ7IiBkPSJNNDUwLjA2MSw0MjIuOTU3SDYxLjkzOUMxMDguODg2LDQ3Ny40NzYsMTc4LjQwOSw1MTIsMjU2LDUxMlM0MDMuMTE0LDQ3Ny40NzYsNDUwLjA2MSw0MjIuOTU3eiIvPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=
-- Chile
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPGNpcmNsZSBzdHlsZT0iZmlsbDojRjBGMEYwOyIgY3g9IjI1NiIgY3k9IjI1NiIgcj0iMjU2Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiNEODAwMjc7IiBkPSJNNTEyLDI1NmMwLDE0MS4zODQtMTE0LjYxNiwyNTYtMjU2LDI1NlMwLDM5Ny4zODQsMCwyNTZzMjU2LDAsMjU2LDBTNDQ5Ljc2MSwyNTYsNTEyLDI1NnoiLz4KPHBhdGggc3R5bGU9ImZpbGw6IzAwNTJCNDsiIGQ9Ik0wLDI1NkMwLDExNC42MTYsMTE0LjYxNiwwLDI1NiwwYzAsOTcuMzU1LDAsMjU2LDAsMjU2Uzg5LjA0MywyNTYsMCwyNTZ6Ii8+Cjxwb2x5Z29uIHN0eWxlPSJmaWxsOiNGMEYwRjA7IiBwb2ludHM9IjE1Mi4zODksODkuMDQzIDE2OC45NjYsMTQwLjA2MSAyMjIuNjA5LDE0MC4wNjEgMTc5LjIxMSwxNzEuNTkxIDE5NS43ODcsMjIyLjYwOSAgIDE1Mi4zODksMTkxLjA3OCAxMDguOTkxLDIyMi42MDkgMTI1LjU2NywxNzEuNTkxIDgyLjE2OSwxNDAuMDYxIDEzNS44MTIsMTQwLjA2MSAiLz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==
-- Brasil
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPGNpcmNsZSBzdHlsZT0iZmlsbDojNkRBNTQ0OyIgY3g9IjI1NiIgY3k9IjI1NiIgcj0iMjU2Ii8+Cjxwb2x5Z29uIHN0eWxlPSJmaWxsOiNGRkRBNDQ7IiBwb2ludHM9IjI1NiwxMDAuMTc0IDQ2Ny40NzgsMjU2IDI1Niw0MTEuODI2IDQ0LjUyMiwyNTYgIi8+CjxjaXJjbGUgc3R5bGU9ImZpbGw6I0YwRjBGMDsiIGN4PSIyNTYiIGN5PSIyNTYiIHI9Ijg5LjA0MyIvPgo8Zz4KCTxwYXRoIHN0eWxlPSJmaWxsOiMwMDUyQjQ7IiBkPSJNMjExLjQ3OCwyNTAuNDM1Yy0xNS40ODQsMC0zMC40MjcsMi4zNTUtNDQuNDkzLDYuNzI1YzAuNjIzLDQ4LjY0LDQwLjIyNyw4Ny44ODQsODkuMDE1LDg3Ljg4NCAgIGMzMC4xNjgsMCw1Ni44MTItMTUuMDE3LDcyLjkxOS0zNy45NjhDMzAxLjM2MiwyNzIuNTc5LDI1OC45NjEsMjUwLjQzNSwyMTEuNDc4LDI1MC40MzV6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojMDA1MkI0OyIgZD0iTTM0My4zOTMsMjczLjA2YzEuMDcyLTUuNTI0LDEuNjUxLTExLjIyMywxLjY1MS0xNy4wNmMwLTQ5LjE3OC0zOS44NjYtODkuMDQzLTg5LjA0My04OS4wNDMgICBjLTM2LjY5NCwwLTY4LjE5NCwyMi4yMDEtODEuODI2LDUzLjg5OWMxMi4wNS0yLjQ5NywyNC41MjYtMy44MTIsMzcuMzA1LTMuODEyQzI2My4xOTcsMjE3LjA0MywzMDkuOTgzLDIzOC41NDEsMzQzLjM5MywyNzMuMDZ6Ii8+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==
-- Uruaguay
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPGNpcmNsZSBzdHlsZT0iZmlsbDojRjBGMEYwOyIgY3g9IjI1NiIgY3k9IjI1NiIgcj0iMjU2Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMCIgc3R5bGU9ImZpbGw6IzMzOEFGMzsiIGQ9Ik0yNTYsMTg5LjIxN2gyNDcuMTgxYy02LjQxOS0yMy44MTQtMTYuMTc1LTQ2LjI1NS0yOC43NTUtNjYuNzgzSDI1NlYxODkuMjE3eiIvPgo8Zz4KCTxwYXRoIHN0eWxlPSJmaWxsOiMzMzhBRjM7IiBkPSJNOTYuNjQzLDQ1Ni4zNDhoMzE4LjcxM2MyMy4zNjMtMTguNjA4LDQzLjM5OS00MS4yMSw1OS4wNjktNjYuNzgzSDM3LjU3NCAgIEM1My4yNDUsNDE1LjEzNyw3My4yODEsNDM3Ljc0LDk2LjY0Myw0NTYuMzQ4eiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6IzMzOEFGMzsiIGQ9Ik0yNTYsMGMwLDIyLjI2MSwwLDU1LjY1MiwwLDU1LjY1MmgxNTkuMzU3QzM3MS42MjcsMjAuODI0LDMxNi4yNDksMCwyNTYsMHoiLz4KPC9nPgo8Zz4KCTxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMF8xXyIgc3R5bGU9ImZpbGw6IzMzOEFGMzsiIGQ9Ik0yNTYsMTg5LjIxN2gyNDcuMTgxYy02LjQxOS0yMy44MTQtMTYuMTc1LTQ2LjI1NS0yOC43NTUtNjYuNzgzSDI1NiAgIFYxODkuMjE3eiIvPgo8L2c+CjxwYXRoIHN0eWxlPSJmaWxsOiMzMzhBRjM7IiBkPSJNMCwyNTZjMCwyMy4xMDcsMy4wOCw0NS40ODksOC44MTksNjYuNzgzaDQ5NC4zNjNDNTA4LjkyLDMwMS40ODksNTEyLDI3OS4xMDcsNTEyLDI1NkgweiIvPgo8cG9seWdvbiBzdHlsZT0iZmlsbDojRkZEQTQ0OyIgcG9pbnRzPSIyMjIuNjA5LDE0OS44MjEgMTkxLjM0MywxNjQuNTI4IDIwNy45OTIsMTk0LjgwOCAxNzQuMDQyLDE4OC4zMTQgMTY5Ljc0LDIyMi42MDkgICAxNDYuMDk0LDE5Ny4zODUgMTIyLjQ0NiwyMjIuNjA5IDExOC4xNDUsMTg4LjMxNCA4NC4xOTUsMTk0LjgwNiAxMDAuODQzLDE2NC41MjcgNjkuNTc5LDE0OS44MjEgMTAwLjg0NCwxMzUuMTE2IDg0LjE5NSwxMDQuODM2ICAgMTE4LjE0NCwxMTEuMzMgMTIyLjQ0Nyw3Ny4wMzUgMTQ2LjA5NCwxMDIuMjU5IDE2OS43NDEsNzcuMDM1IDE3NC4wNDIsMTExLjMzIDIwNy45OTMsMTA0LjgzNiAxOTEuMzQ0LDEzNS4xMTcgIi8+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=
-- Argentina
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPGNpcmNsZSBzdHlsZT0iZmlsbDojRjBGMEYwOyIgY3g9IjI1NiIgY3k9IjI1NiIgcj0iMjU2Ii8+CjxnPgoJPHBhdGggc3R5bGU9ImZpbGw6IzMzOEFGMzsiIGQ9Ik0yNTYsMEMxNTQuNTA2LDAsNjYuODEsNTkuMDY1LDI1LjQwMiwxNDQuNjk2aDQ2MS4xOTVDNDQ1LjE5LDU5LjA2NSwzNTcuNDkzLDAsMjU2LDB6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojMzM4QUYzOyIgZD0iTTI1Niw1MTJjMTAxLjQ5MywwLDE4OS4xOS01OS4wNjUsMjMwLjU5OC0xNDQuNjk2SDI1LjQwMkM2Ni44MSw0NTIuOTM1LDE1NC41MDYsNTEyLDI1Niw1MTJ6Ii8+CjwvZz4KPHBvbHlnb24gc3R5bGU9ImZpbGw6I0ZGREE0NDsiIHBvaW50cz0iMzMyLjUxNSwyNTYgMzAxLjI1LDI3MC43MDcgMzE3Ljg5OSwzMDAuOTg2IDI4My45NDksMjk0LjQ5MSAyNzkuNjQ3LDMyOC43ODcgMjU2LDMwMy41NjMgICAyMzIuMzUyLDMyOC43ODcgMjI4LjA1MSwyOTQuNDkxIDE5NC4xMDEsMzAwLjk4NSAyMTAuNzQ5LDI3MC43MDYgMTc5LjQ4NSwyNTYgMjEwLjc1LDI0MS4yOTMgMTk0LjEwMSwyMTEuMDE1IDIyOC4wNSwyMTcuNTA5ICAgMjMyLjM1MywxODMuMjEzIDI1NiwyMDguNDM3IDI3OS42NDgsMTgzLjIxMyAyODMuOTQ5LDIxNy41MDkgMzE3LjksMjExLjAxNSAzMDEuMjUxLDI0MS4yOTQgIi8+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=
-- Venezuela
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPHBhdGggc3R5bGU9ImZpbGw6IzAwNTJCNDsiIGQ9Ik00ODYuNTk4LDM2Ny4zMDRDNTAyLjg3MywzMzMuNjQ4LDUxMiwyOTUuODkxLDUxMiwyNTZzLTkuMTI3LTc3LjY0OC0yNS40MDItMTExLjMwNEwyNTYsMTIyLjQzNSAgTDI1LjQwMiwxNDQuNjk2QzkuMTI3LDE3OC4zNTIsMCwyMTYuMTA5LDAsMjU2czkuMTI3LDc3LjY0OCwyNS40MDIsMTExLjMwNEwyNTYsMzg5LjU2NUw0ODYuNTk4LDM2Ny4zMDR6Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiNEODAwMjc7IiBkPSJNMjU2LDUxMmMxMDEuNDkzLDAsMTg5LjE5LTU5LjA2NSwyMzAuNTk4LTE0NC42OTZIMjUuNDAyQzY2LjgxLDQ1Mi45MzUsMTU0LjUwNiw1MTIsMjU2LDUxMnoiLz4KPGc+Cgk8cG9seWdvbiBzdHlsZT0iZmlsbDojRjBGMEYwOyIgcG9pbnRzPSI0NDMuMzY3LDMwNi4yNTIgNDI5Ljg0NiwzMTYuODE4IDQzNS43MTQsMzMyLjkzOSA0MjEuNDg3LDMyMy4zNDYgNDA3Ljk2NSwzMzMuOTEgICAgNDEyLjY5MywzMTcuNDE1IDM5OC40NjgsMzA3LjgxOSA0MTUuNjE3LDMwNy4yMjEgNDIwLjM0OSwyOTAuNzI2IDQyNi4yMTksMzA2Ljg1ICAiLz4KCTxwb2x5Z29uIHN0eWxlPSJmaWxsOiNGMEYwRjA7IiBwb2ludHM9IjQwOC43NDEsMjQ2LjI4MiAzOTkuNjUsMjYwLjgzNSA0MTAuNjgxLDI3My45NzcgMzk0LjAzLDI2OS44MzEgMzg0LjkzNiwyODQuMzggICAgMzgzLjczNywyNjcuMjYzIDM2Ny4wODgsMjYzLjExIDM4MywyNTYuNjgxIDM4MS44MDQsMjM5LjU2MyAzOTIuODMzLDI1Mi43MSAgIi8+Cgk8cG9seWdvbiBzdHlsZT0iZmlsbDojRjBGMEYwOyIgcG9pbnRzPSIzNTUuNjk0LDIwMS43NjkgMzUyLjEyOCwyMTguNTU0IDM2Ni45ODgsMjI3LjEzNCAzNDkuOTIzLDIyOC45MzMgMzQ2LjM1NSwyNDUuNzEzICAgIDMzOS4zNzUsMjMwLjAzOSAzMjIuMzA4LDIzMS44MyAzMzUuMDYyLDIyMC4zNDggMzI4LjA4MywyMDQuNjc0IDM0Mi45NDMsMjEzLjI1MiAgIi8+Cgk8cG9seWdvbiBzdHlsZT0iZmlsbDojRjBGMEYwOyIgcG9pbnRzPSIyOTAuNjIyLDE3OC4wODcgMjkzLjAxMywxOTUuMDggMzA5LjkxLDE5OC4wNTggMjk0LjQ4OCwyMDUuNTg0IDI5Ni44NzcsMjIyLjU3NiAgICAyODQuOTU3LDIxMC4yMzIgMjY5LjUzMSwyMTcuNzU0IDI3Ny41ODksMjAyLjYwMSAyNjUuNjY4LDE5MC4yNTkgMjgyLjU2OCwxOTMuMjM4ICAiLz4KCTxwb2x5Z29uIHN0eWxlPSJmaWxsOiNGMEYwRjA7IiBwb2ludHM9IjIyMS4zNzcsMTc4LjA4NyAyMjkuNDM0LDE5My4yMzggMjQ2LjMzLDE5MC4yNTcgMjM0LjQxMywyMDIuNjA0IDI0Mi40NjYsMjE3Ljc1NiAgICAyMjcuMDQ0LDIxMC4yMzIgMjE1LjEyMiwyMjIuNTczIDIxNy41MTMsMjA1LjU4NCAyMDIuMDksMTk4LjA2MiAyMTguOTg5LDE5NS4wOCAgIi8+Cgk8cG9seWdvbiBzdHlsZT0iZmlsbDojRjBGMEYwOyIgcG9pbnRzPSIxNTYuMzA1LDIwMS43NjkgMTY5LjA1OSwyMTMuMjUyIDE4My45MTYsMjA0LjY3NCAxNzYuOTM5LDIyMC4zNTQgMTg5LjY5LDIzMS44MzQgICAgMTcyLjYyNSwyMzAuMDM5IDE2NS42NDMsMjQ1LjcxMyAxNjIuMDgsMjI4LjkzIDE0NS4wMTQsMjI3LjEzOCAxNTkuODc0LDIxOC41NTYgICIvPgoJPHBvbHlnb24gc3R5bGU9ImZpbGw6I0YwRjBGMDsiIHBvaW50cz0iMTAzLjI1OCwyNDYuMjgyIDExOS4xNzEsMjUyLjcxIDEzMC4xOTcsMjM5LjU2NiAxMjkuMDA0LDI1Ni42ODcgMTQ0LjkxMiwyNjMuMTE2ICAgIDEyOC4yNjMsMjY3LjI2NSAxMjcuMDY0LDI4NC4zOCAxMTcuOTc0LDI2OS44MzEgMTAxLjMyMywyNzMuOTggMTEyLjM1MywyNjAuODM1ICAiLz4KCTxwb2x5Z29uIHN0eWxlPSJmaWxsOiNGMEYwRjA7IiBwb2ludHM9IjY4LjYzNCwzMDYuMjUyIDg1Ljc4NiwzMDYuODUgOTEuNjUyLDI5MC43MjggOTYuMzg3LDMwNy4yMjQgMTEzLjUzNCwzMDcuODIzICAgIDk5LjMwOCwzMTcuNDE5IDEwNC4wMzYsMzMzLjkxMyA5MC41MTYsMzIzLjM0OSA3Ni4yODksMzMyLjk0MiA4Mi4xNTgsMzE2LjgxOCAgIi8+CjwvZz4KPHBhdGggc3R5bGU9ImZpbGw6I0ZGREE0NDsiIGQ9Ik00ODYuNTk4LDE0NC42OTZDNDQ1LjE5LDU5LjA2NSwzNTcuNDkzLDAsMjU2LDBDMTU0LjUwNiwwLDY2LjgxLDU5LjA2NSwyNS40MDIsMTQ0LjY5Nkg0ODYuNTk4eiIvPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8Zz4KPC9nPgo8L3N2Zz4K
-- Bolivia
-- data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPHBhdGggc3R5bGU9ImZpbGw6I0ZGREE0NDsiIGQ9Ik01MTIsMjU2YzAtMzEuMzE0LTUuNjMyLTYxLjMxMS0xNS45MjMtODkuMDQzTDI1NiwxNTUuODI2bC0yNDAuMDc3LDExLjEzICBDNS42MzIsMTk0LjY4OSwwLDIyNC42ODYsMCwyNTZzNS42MzIsNjEuMzExLDE1LjkyMyw4OS4wNDNMMjU2LDM1Ni4xNzRsMjQwLjA3Ny0xMS4xM0M1MDYuMzY4LDMxNy4zMTEsNTEyLDI4Ny4zMTQsNTEyLDI1NnoiLz4KPHBhdGggc3R5bGU9ImZpbGw6IzZEQTU0NDsiIGQ9Ik0yNTYsNTEyYzExMC4wNzEsMCwyMDMuOTA2LTY5LjQ3MiwyNDAuMDc3LTE2Ni45NTdIMTUuOTIzQzUyLjA5NCw0NDIuNTI4LDE0NS45MjksNTEyLDI1Niw1MTJ6Ii8+CjxwYXRoIHN0eWxlPSJmaWxsOiNEODAwMjc7IiBkPSJNMTUuOTIzLDE2Ni45NTdoNDgwLjE1NUM0NTkuOTA2LDY5LjQ3MiwzNjYuMDcxLDAsMjU2LDBTNTIuMDk0LDY5LjQ3MiwxNS45MjMsMTY2Ljk1N3oiLz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==



